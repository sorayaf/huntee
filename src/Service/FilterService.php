<?php 

namespace App\Service;

use App\Entity\Hunt;

class FilterService {

    public function createHuntFilteredData($hunt)
    {
        $data = [
            'id' => $hunt->getId(), 
            'name'=>$hunt->getName(), 
            'description'=>$hunt->getDescription(), 
            'image'=>$hunt->getImage(),
            'beginDate'=>$hunt->getBeginDate()->format('d-m-Y'), 
            'endDate'=>$hunt->getEndDate()?$hunt->getEndDate()->format('d-m-Y'):null, 
            'entryFee'=>$hunt->getEntryFee(), 
            'prize'=>$hunt->getPrize()?$hunt->getPrize()->getType().', '.$hunt->getPrize()->getDescription():null
        ]; 
       return $data;
    }
}