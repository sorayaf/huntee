<?php 

namespace App\Service;

class DateService {

    public function getTimeEurope() {
        $date = new \Datetime(); 
        return $date->setTimezone(new \DateTimeZone('Europe/Paris')); 
    }
}