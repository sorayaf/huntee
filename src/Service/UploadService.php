<?php 

namespace App\Service;

use Symfony\Component\String\Slugger\SluggerInterface;


class UploadService {


    private $slugger; 
    
    public function __construct(SluggerInterface $slugger){
        $this->slugger = $slugger; 
    }


    public function renameFile($file, $directory)
    {
        $name = ''; 
        if($file !== null)
        {
            $extension = $file->guessExtension(); 
            if(!$extension)
            {
                $extension = 'bin'; 
            }
    
            $name = $this->slugger->slug($file->getClientOriginalName()).'-'.uniqid().'.'.$extension;
            $file->move($directory, $name);  
        }

        return $name;
    }
}