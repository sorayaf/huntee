<?php

namespace App\Controller;

use App\Entity\Hunt;
use App\Form\HuntType;
use App\Entity\HuntComment;
use App\Service\DateService;
use App\Form\HuntCommentType;
use App\Service\UploadService;
use App\Form\HuntCommentEditType;
use App\Repository\HuntRepository;
use App\Repository\RegionRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/hunt', name: 'hunt')]
class HuntController extends AbstractController
{
    #[Route('/', name: '_index', methods: ['GET'])]
    public function index(Request $request, HuntRepository $huntRepo, PaginatorInterface $paginator, RegionRepository $regionRepo, CategoryRepository $catRepo): Response
    {   
        $hunts = $huntRepo->findBy(['isPublished' => true], ['beginDate' => 'DESC']); 
        $hunts = $paginator->paginate(
            $hunts, 
            $request->query->getInt('page', 1), 
            6
        ); 

        return $this->render('hunt/index.html.twig', [
            'hunts' => $hunts, 
            'regions' => $regionRepo->findAll(), 
            'categories' => $catRepo->findAll(), 
        ]);
    }

    #[Route('/new', name: '_new', methods: ['GET', 'POST'])]
    public function new(CategoryRepository $catRepo, Request $request, EntityManagerInterface $manager, DateService $dateService, UploadService $uploadService) : Response
    {
        $hunt = new Hunt(); 

        $form = $this->createForm(HuntType::class, $hunt); 
        $date = $dateService->getTimeEurope(); 
        
        if($form->handleRequest($request)->isSubmitted() && $form->isValid())
        {

            $hunt
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
            ->setisPublished(false)
            ->setUser($this->getUser()); 
            
            $image = $uploadService->renameFile($form['image']->getData(), $this->getParameter('hunts_images_directory')); 

            $image ? $hunt->setImage($image): $hunt->setImage('no-hunt-image.jpeg'); 

            if($hunt->getEntryFee() === 0.0)
            {
                $hunt->addCategory($catRepo->findOneBy(['name'=>'Gratuit'])); 
            }
            else 
            {
                $hunt->addCategory($catRepo->findOneBy(['name'=>'Payant'])); 
            }

            if($hunt->getEndDate() === null)
            {
                $hunt->addCategory($catRepo->findOneBy(['name'=>'Permanent'])); 
            }
            else
            {
                $hunt->addCategory($catRepo->findOneBy(['name'=>'Temporaire'])); 
            }

            if($hunt->getDepartment() === null)
            {
                $hunt->setIsLocated(true); 
            }
            else {
                $hunt->setIsLocated(false); 
            }

            $manager->persist($hunt); 
            $manager->flush(); 
            
            return $this->render('hunt/created.html.twig', []);
        }
        
        
        return $this->renderForm('hunt/new.html.twig', [
            'hunt' => $hunt, 
            'form' => $form
        ]);
    }

    #[Route('/{id}', name: '_show', methods: ['GET', 'POST'])]
    public function show(EntityManagerInterface $manager, Request $request, Hunt $hunt=null, $id, DateService $dateService): Response
    {
        if($hunt == null)
        {
            return $this->redirectToRoute('app_index'); 
        }

        $comment = new HuntComment();

        $form = $this->createForm(HuntCommentType::class, $comment);
        $date = $dateService->getTimeEurope(); 

        if($form->handleRequest($request)->isSubmitted() && $form->isValid())
        {
            $comment
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
            ->setUser($this->getUser())
            ->setHunt($hunt); 

            $manager->persist($comment); 
            $manager->flush(); 

            return $this->redirect($request->getUri()); 
        }

        
        return $this->renderForm('hunt/show.html.twig', [
            'form' => $form, 
            'hunt' => $hunt,
            'comments' => $hunt->getComments(),
            'news' => $hunt->getNews(), 
        ]); 
    }

    #[Route('/api/comment/edit/{id}/{message}', name: '_edit_comment', methods: ['GET', 'POST'])]
    public function editComment(EntityManagerInterface $manager, Request $request, $message, HuntComment $comment, DateService $dateService,)
    {
        $date = $dateService->getTimeEurope(); 

        $comment
            ->setUpdatedAt($date)
            ->setContent($message); 

        $manager->flush(); 

        return new JsonResponse($message); 
    }


}

