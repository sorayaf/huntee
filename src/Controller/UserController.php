<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\HuntRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


#[Route('/user', name: 'user')]
class UserController extends AbstractController
{
    #[Route('/{id}', name: '_show', methods: ['GET'])]
    public function show(User $user, $id, HuntRepository $huntRepo): Response
    {
            $huntsPublished = $huntRepo->findBy(['user' => $user->getId(), 'isPublished' => true]); 

            $huntsNotPublished = $huntRepo->findBy(['user' => $user->getId(), 'isPublished' => false]); 

            $userComments = count($user->getNewsComments()) + count($user->getHuntComments()); 

            return $this->render('user/show.html.twig', [
                'user' => $user,
                'huntsPublished' => $huntsPublished, 
                'huntsNotPublished' => $huntsNotPublished, 
                'comments' => $userComments
            ]);
       
    }

    #[Route('/{id}/edit', name: '_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {

        $userConnected = $this->getUser(); 
        if(($user->getId() !== $this->getUser()->getId()) &&  !in_array('ROLE_ADMIN', $userConnected->getRoles())) {
            return $this->redirectToRoute('app_index'); 
        }           
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($form->get('password')->getData() !== null) {

                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                            $user,
                            $form->get('password')->getData()
                        )
                    );
            }

            $entityManager->flush();

            return $this->redirectToRoute('user_show', ['id' => $user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: '_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $this->container->get('security.token_storage')->setToken(null);
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_index', [], Response::HTTP_SEE_OTHER);
    }
}
