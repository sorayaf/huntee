<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\NewsComment;
use App\Service\DateService;
use App\Form\NewsCommentType;

use Symfony\Component\Mime\Email;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/news', name: 'news')]
class NewsController extends AbstractController
{

    #[Route('', name: '_index', methods: ['GET'])]
    public function index(NewsRepository $news, Request $request, PaginatorInterface $paginator): Response
    {
        $news = $news->findBy(['isPublished' => true], ['createdAt' => 'ASC']); 
        $news = $paginator->paginate(
            $news, 
            $request->query->getInt('page', 1), 
            4
        ); 


        return $this->render('news/index.html.twig', [
            'allNews' => $news
        ]);
          
    }


    #[Route('/{id}', name: '_show', methods: ['GET', 'POST'])]
    public function show(EntityManagerInterface $manager, Request $request, News $news=null, $id, DateService $dateService): Response
    {
        if($news == null)
        {
            return $this->redirectToRoute('app_index'); 
        }

        $comment = new NewsComment();

        $form = $this->createForm(NewsCommentType::class, $comment);
        $date = $dateService->getTimeEurope(); 

        if($form->handleRequest($request)->isSubmitted() && $form->isValid())
        {
            $comment
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
            ->setUser($this->getUser())
            ->setNews($news); 

            $manager->persist($comment); 
            $manager->flush(); 

            return $this->redirect($request->getUri()); 
        }

        
        return $this->renderForm('news/show.html.twig', [
            'form' => $form, 
            'news' => $news,
            'comments' => $news->getComments(),
        ]); 
    }

    #[Route('/api/comment/edit/{id}/{message}', name: '_edit_comment', methods: ['GET', 'POST'])]
    public function editComment(EntityManagerInterface $manager, Request $request, $message, NewsComment $comment, DateService $dateService,)
    {
        $date = $dateService->getTimeEurope(); 

        $comment
            ->setUpdatedAt($date)
            ->setContent($message); 

        $manager->flush(); 

        return new JsonResponse($message); 
    }

}
