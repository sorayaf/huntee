<?php

namespace App\Controller\Admin;

use App\Entity\Hunt;
use App\Entity\News;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\ImageNews;
use App\Entity\HuntComment;
use App\Entity\NewsComment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

//#[IsGranted("ROLE_ADMIN")]
class DashboardController extends AbstractDashboardController
{

    public function __construct(private AdminUrlGenerator $adminUrlGenerator){

    }
    
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        $url = $this->adminUrlGenerator
        ->setController(UserCrudController::class)
        ->generateUrl();

        return $this->redirect($url);

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Huntee');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'), 

            MenuItem::section('Utilisateurs'),
            MenuItem::linkToCrud('Liste', 'fas fa-list', User::class),

            MenuItem::section('Publications'),
            MenuItem::subMenu('Actualités', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Liste', 'fas fa-list', News::class),  
                MenuItem::linkToCrud('Commentaires', 'fas fa-list', NewsComment::class), 
            ]), 

            MenuItem::subMenu('Chasses au trésor', 'fa fa-article')->setSubItems([
                MenuItem::linkToCrud('Liste', 'fas fa-list', Hunt::class),  
                MenuItem::linkToCrud('Commentaires', 'fas fa-list', HuntComment::class), 
            ]), 

            MenuItem::section('Autres'),
            MenuItem::linkToCrud('Catégories', 'fas fa-list', Category::class), 
        ]; 
    }
}
