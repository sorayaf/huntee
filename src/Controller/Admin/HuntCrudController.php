<?php

namespace App\Controller\Admin;

use App\Entity\Hunt;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class HuntCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Hunt::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $hunt = new Hunt(); 
        $hunt->setUser($this->getUser())
            ->setCreatedAt($date)
            ->setUpdatedAt($date); 

        if($hunt->getDepartment() === null) {
            $hunt->setIsLocated(true); 
        }
        else {
            $hunt->setIslocated(false); 
        }
        return $hunt; 
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance) : void
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $entityInstance->setUpdatedAt($date); 
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }
    

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'), 
            ImageField::new('image')
            ->setBasePath('/uploads/hunts')
            ->setUploadDir('/public/uploads/hunts/')
            ->setUploadedFileNamePattern("[slug]-[contenthash].[extension]")
            ->setSortable(false)
            ->setFormTypeOption('required' ,false),
            TextEditorField::new('description'),
            DateTimeField::new('beginDate'), 
            DateTimeField::new('endDate')->setFormTypeOption('required' ,false), 
            BooleanField::new('isPayable'),
            NumberField::new('entryFee'), 
            AssociationField::new('category')->setTemplatePath('admin/category/category.html.twig'),
            AssociationField::new('department')->setTemplatePath('admin/department/department.html.twig'),
            AssociationField::new('prize')->setFormTypeOption('disabled','disabled'), 
            AssociationField::new('news')->autocomplete(),
            BooleanField::new('isPublished'),
            AssociationField::new('comments')->renderAsNativeWidget()->setFormTypeOption('disabled','disabled')
        ];
    }
}
