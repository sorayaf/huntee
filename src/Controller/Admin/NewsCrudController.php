<?php

namespace App\Controller\Admin;

use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class NewsCrudController extends AbstractCrudController
{

   
    public function createEntity(string $entityFqcn)
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $news = new News(); 
        $news->setUser($this->getUser())
            ->setCreatedAt($date)
            ->setUpdatedAt($date); 

        return $news; 
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance) : void
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $entityInstance->setUpdatedAt($date); 
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }
    

    public static function getEntityFqcn(): string
    {
        return News::class;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title'),
            ImageField::new('image')
            ->setBasePath('/uploads/news')
            ->setUploadDir('/public/uploads/news/')
            ->setUploadedFileNamePattern("[slug]-[contenthash].[extension]")
            ->setSortable(false)
            ->setFormTypeOption('required' ,false),
            TextareaField::new('content')->setFormType(CKEditorType::class)->hideOnIndex(),
            AssociationField::new('category')->setTemplatePath('admin/category/category.html.twig'),
            AssociationField::new('hunt')->autocomplete(),
            BooleanField::new('isPublished'),
            AssociationField::new('comments')->setFormTypeOption('disabled','disabled')
        ];

        
    }

    public function configureCrud(Crud $crud): Crud
{
  return $crud
      ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
}
    
}
