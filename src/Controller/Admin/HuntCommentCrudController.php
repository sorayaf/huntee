<?php

namespace App\Controller\Admin;

use App\Entity\HuntComment;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class HuntCommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HuntComment::class;
    }

     
    public function createEntity(string $entityFqcn)
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $news = new HuntComment(); 
        $news->setUser($this->getUser())
            ->setCreatedAt($date)
            ->setUpdatedAt($date); 

        return $news; 
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance) : void
    {
        $date = new \Datetime(); 
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $entityInstance->setUpdatedAt($date); 
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }
    

   
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('user')->hideOnForm(),
            AssociationField::new('hunt')->hideOnForm(),
            TextField::new('content')->setMaxLength(15), 
            TextEditorField::new('content'),
            DateTimeField::new('createdAt'),
            DateTimeField::new('updatedAt')
        ];
    }
    
}
