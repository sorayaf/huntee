<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureActions(Actions $actions): Actions
    {
    return $actions
        ->remove(Crud::PAGE_INDEX, Action::NEW)
    ;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            EmailField::new("email"), 
            TextField::new("pseudo"), 
            ChoiceField::new("roles")->setChoices([
                     'ROLE_USER' => 'ROLE_USER',  
                     'ROLE_ADMIN' => 'ROLE_ADMIN', 
            ])->allowMultipleChoices(), 
            ImageField::new('avatar')
            ->setBasePath('/uploads/avatars')
            ->setUploadDir('/public/uploads/avatars/')
            ->setUploadedFileNamePattern("[slug]-[contenthash].[extension]")
            ->setSortable(false)
            ->setFormTypeOption('required' ,false),
        ];
    }
    

}
