<?php

namespace App\Controller;

use App\Service\FilterService;
use App\Repository\HuntRepository;
use App\Repository\RegionRepository;
use App\Repository\CategoryRepository;
use App\Repository\DepartmentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/api/filter', name: 'filter')]
class FilterController extends AbstractController
{

  #[Route('/', name: '_hunts_filter', methods: ['GET', 'POST'])]
  public function filterAllHunts(HuntRepository $huntRepo, FilterService $filterService, Request $request): Response
  {
      $date = $request->get('date');
      $dpt = $request->get('dpt'); 
      $cat =  explode(',', $request->get('cat'));  
      $huntsFiltered = []; 
      $hunts = []; 
       
      $hunts = $huntRepo->filterAll($date, $dpt, $cat); 

      foreach($hunts as $hunt) 
      {
          $eachObject = $filterService->createHuntFilteredData($hunt);
          $huntsFiltered[] = $eachObject; 
      }

      return new JsonResponse($huntsFiltered);
  }

    #[Route('/regions', name: '_hunts_filter_region', methods: ['GET', 'POST'])]
    public function showDptByRegion(RegionRepository $regionRepo, DepartmentRepository $dptRepo, Request $request): Response
    {
      $dptList = []; 
      $region = $request->get('region'); 
      if($region === 'FR')
      {
        return new JsonResponse('FR');
      }
      else{
        $regions = $regionRepo->findAll(); 
        $regionId = $regionRepo->findBy(['name' => $region])[0]->getId(); 
        $dptsInRegion = $dptRepo->findBy(['region' => $regionId]); 
        foreach($dptsInRegion as $dpt)
        {
          $dpts = ['code'=>$dpt->getCode(), 'name'=>$dpt->getName()]; 
          $dptList[] = $dpts; 
        }
        return new JsonResponse($dptList);
      }
    }
}
