<?php

namespace App\Controller;

use App\Repository\HuntRepository;
use App\Repository\NewsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(HuntRepository $huntRepo, NewsRepository $newsRepo): Response
    {
        $latestHunts = $huntRepo->findBy(['isPublished' => true], ['beginDate' => 'DESC'], limit:6);   
        $latestNews = $newsRepo->findBy(['isPublished' => true], ['createdAt' => 'DESC'], limit:4);     
        return $this->render('home/index.html.twig', [
            'latestHunts' => $latestHunts,
            'latestNews' => $latestNews
        ]);
    }

    #[Route('/api-search/{val}', name: 'api-search')]
    public function searchAPI(HuntRepository $huntRepo, NewsRepository $newsRepo, string $val): Response
    {
        $hunts = $huntRepo->findAllLike($val);
        $news = $newsRepo->findAllLike($val); 
        
        $results = array_merge($hunts, $news); 
        $datas = [];
        foreach ($results as $result) 
        {
            if(property_exists($result, 'name')) 
            {
                $data = ["hunt" => [
                    "id" => $result->getId(),
                    "name" => $result->getName(), 
                    "image" => "/uploads/hunts/". $result->getImage(), 
                    "description" => $result->getDescription(),
                ]]; 
            }
            else 
            {
                $data = ["news" => [
                    "id" => $result->getId(),
                    "title" => $result->getTitle(), 
                    "image" => "/uploads/news/" . $result->getImage(), 
                    "content" => $result->getContent(),  
                ]]; 
            }
            $datas[] = $data; 
        }

        return new JsonResponse($datas);
    }
}
