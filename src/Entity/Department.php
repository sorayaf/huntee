<?php

namespace App\Entity;

use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DepartmentRepository::class)]
class Department
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 3)]
    private $code;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: Region::class, inversedBy: 'departments')]
    private $region;

    #[ORM\ManyToMany(targetEntity: Hunt::class, mappedBy: 'department')]
    private $hunts;

    public function __construct()
    {
        $this->hunts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection<int, Hunt>
     */
    public function getHunts(): Collection
    {
        return $this->hunts;
    }

    public function addHunt(Hunt $hunt): self
    {
        if (!$this->hunts->contains($hunt)) {
            $this->hunts[] = $hunt;
            $hunt->addDepartment($this);
        }

        return $this;
    }

    public function removeHunt(Hunt $hunt): self
    {
        if ($this->hunts->removeElement($hunt)) {
            $hunt->removeDepartment($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name; 
    }
}
