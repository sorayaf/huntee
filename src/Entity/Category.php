<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Hunt::class, mappedBy: 'category')]
    private $hunts;

    #[ORM\ManyToMany(targetEntity: News::class, mappedBy: 'category')]
    private $news;

    public function __construct()
    {
        $this->hunts = new ArrayCollection();
        $this->news = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Hunt>
     */
    public function getHunts(): Collection
    {
        return $this->hunts;
    }

    public function addHunt(Hunt $hunt): self
    {
        if (!$this->hunts->contains($hunt)) {
            $this->hunts[] = $hunt;
            $hunt->addCategory($this);
        }

        return $this;
    }

    public function removeHunt(Hunt $hunt): self
    {
        if ($this->hunts->removeElement($hunt)) {
            $hunt->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, News>
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->addCategory($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->removeElement($news)) {
            $news->removeCategory($this);
        }

        return $this;
    }
    
    public function __toString()
    {
        return $this->name; 
    }
}
