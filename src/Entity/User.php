<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'Un compte avec ce mail existe déjà')]
#[UniqueEntity(fields: ['pseudo'], message: 'Ce pseudo est déjà pris')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\Email(
        message: 'L\'email n\'est pas valide',
    )]
    #[Assert\NotBlank(
        message: 'Merci de renseigner un mail',
    )]
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[Assert\Regex(
        pattern: '/^[a-z0-9_-]+$/i',
        htmlPattern: '^[a-zA-Z0-9_-]+$', 
        message : 'Le pseudo n\'est pas valide (caractères autorisés : lettres, chiffres, - et _)'
    )]
    #[Assert\NotBlank(
        message: 'Merci de renseigner un pseudo',
    )]
    #[ORM\Column(type: 'string', length: 255)]
    private $pseudo;

    #[ORM\Column(type: 'string')]
    private $password;
    
    #[ORM\Column(type: 'string', length: 255)]
    private $avatar;

    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Hunt::class)]
    private $hunts;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: News::class)]
    private $news;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: NewsComment::class, orphanRemoval: true)]
    private $newsComments;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: HuntComment::class, orphanRemoval: true)]
    private $huntComments;

    public function __construct()
    {
        $this->hunts = new ArrayCollection();
        $this->news = new ArrayCollection();
        $this->newsComments = new ArrayCollection();
        $this->huntComments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, Hunt>
     */
    public function getHunts(): Collection
    {
        return $this->hunts;
    }

    public function addHunt(Hunt $hunt): self
    {
        if (!$this->hunts->contains($hunt)) {
            $this->hunts[] = $hunt;
            $hunt->setUser($this);
        }

        return $this;
    }

    public function removeHunt(Hunt $hunt): self
    {
        if ($this->hunts->removeElement($hunt)) {
            // set the owning side to null (unless already changed)
            if ($hunt->getUser() === $this) {
                $hunt->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, News>
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->setUser($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->removeElement($news)) {
            // set the owning side to null (unless already changed)
            if ($news->getUser() === $this) {
                $news->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NewsComment>
     */
    public function getNewsComments(): Collection
    {
        return $this->newsComments;
    }

    public function addNewsComment(NewsComment $newsComment): self
    {
        if (!$this->newsComments->contains($newsComment)) {
            $this->newsComments[] = $newsComment;
            $newsComment->setUser($this);
        }

        return $this;
    }

    public function removeNewsComment(NewsComment $newsComment): self
    {
        if ($this->newsComments->removeElement($newsComment)) {
            // set the owning side to null (unless already changed)
            if ($newsComment->getUser() === $this) {
                $newsComment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HuntComment>
     */
    public function getHuntComments(): Collection
    {
        return $this->huntComments;
    }

    public function addHuntComment(HuntComment $huntComment): self
    {
        if (!$this->huntComments->contains($huntComment)) {
            $this->huntComments[] = $huntComment;
            $huntComment->setUser($this);
        }

        return $this;
    }

    public function removeHuntComment(HuntComment $huntComment): self
    {
        if ($this->huntComments->removeElement($huntComment)) {
            // set the owning side to null (unless already changed)
            if ($huntComment->getUser() === $this) {
                $huntComment->setUser(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->pseudo; 
    }

}
