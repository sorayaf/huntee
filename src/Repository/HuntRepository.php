<?php

namespace App\Repository;

use App\Entity\Hunt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Hunt>
 *
 * @method Hunt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hunt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hunt[]    findAll()
 * @method Hunt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HuntRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hunt::class);
    }

    public function add(Hunt $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Hunt $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function filterAll($date, $code, $categories = [])
    {
        $qb = $this->createQueryBuilder('h')
        ->select('h')
        ->where('h.isPublished = true'); 

        if($date)
        {
            switch($date)
            {
                case(''):
                    $qb->orderBy('h.createdAt', 'asc');
                break; 
                case 'ascBeginDate':
                    $qb->orderBy('h.beginDate', 'asc');
                break; 
                case 'descBeginDate':
                    $qb->orderBy('h.beginDate', 'desc');
                break; 
            } 
        }

        if($code) 
        {
            $qb->innerJoin('h.department', 'd')
            ->where('d.code = :code')
            ->setParameter('code', $code)
            ->groupBy("h.id"); 
    
        }

        if($categories[0] !== '')
        {
            $qb->innerJoin('h.category', 'c'); 
    
                    $qb
                    ->andWhere('c.name in (:categories)')
                    ->setParameter('categories', $categories)
                    ->groupBy("h.name")
                    ->having('count(distinct c.id) ='.count($categories)); 
                
        }
        
        return $qb->getQuery()->getResult();
    }

    public function findAllLike($value)
    {
        return $this->createQueryBuilder('h')
        ->andWhere('h.isPublished = true')
        ->andWhere('h.name LIKE :val')
        ->setParameter('val', "%" . $value . "%")
        ->setMaxResults(10)
        ->getQuery()
        ->getResult();
    }


//    /**
//     * @return Hunt[] Returns an array of Hunt objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Hunt
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
