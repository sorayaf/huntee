<?php

namespace App\Form;

use DateTime;
use App\Entity\Hunt;
use App\Entity\Prize;
use App\Form\PrizeType;
use App\Entity\Category;
use App\Entity\Department;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class HuntType extends AbstractType
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager; 
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('image', FileType::class, [
                'label' =>false,
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid file extension',
                    ])
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom *'
            ])
            ->add('description', TextAreaType::class, [
                'label' => 'Description *'])

            ->add('beginDate', DateTimeType::class, [
                'years' => range(2020, 2030), 
                'label' => 'Date de début *', 
                'attr' => ['class' => 'form-date'],   
                'constraints'=> [new Constraints\Callback(function($object, ExecutionContextInterface $context) {
                    $end = $context->getRoot()->getData()->getEndDate();
                    $beginning = $object;
                    if ($end === null || $end === '') {
                        return;
                    }
                    if ($end < $beginning) {
                        $context
                            ->buildViolation('La date de fin doit être supérieure à la date de départ')
                            ->addViolation();
                    }
                    
                }),
            ], 
     
            ])

            ->add('endDate', DateTimeType::class, [
                'years' => range(2020, 2030), 
                'required' => false, 
                'label' => 'Date de fin', 
                'attr' => ['class' => 'form-date'], 
            ])

            ->add('isPayable', ChoiceType::class, [
                'label' => 'Faut-il payer pour participer ?',
                'choices'  => [
                    'Non' => false,
                    'Oui' => true,
                ],
            ])
            ->add('entryFee', IntegerType::class, [
                'required' =>false, 
                'attr' => ['placeholder' => 0], 
                'label' => 'Montant en € (ou prix cumulé du matériel)'
            ])

            ->add('department', EntityType::class, [
                'class' => Department::class, 
                'expanded'=> true, 
                'choice_label' => 'name', 
                'multiple' => true, 
                'label' => 'Sélectionner les départements'
                
            ])

            ->add('isLocated', ChoiceType::class, [
                'label' => 'La chasse est- elle disponible uniquement dans certains départements ?',
                'choices'  => [
                    'Non' => true,
                    'Oui' => false,
                    
                ],
            ])

            ->add('prize', PrizeType::class, 
            ['label' => false]); 
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Hunt::class,
        ]);
    }
}
