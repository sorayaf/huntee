document.addEventListener("DOMContentLoaded", () => {
       

  function splitText(text, count){
    const stripedText = text.replace( /(<([^>]+)>)/ig, '');
    return stripedText.slice(0, count) + (text.length > count ? "..." : "");
  }

    const baseUrl = 'http://127.0.0.1:8000'; 
    const search = document.querySelector("#searchInput");
    const divResults = document.querySelector(".show-results");
    const pageContent = document.querySelector(".page-content"); 
    const wrapper = document.createElement("div");
    wrapper.classList.add("wrapper"); 
    const section = document.createElement('section'); 
    section.classList.add('vertical-info'); 

    search.addEventListener("keyup", function (e) {
      e.preventDefault();
      
      //affichage du contenu de base si l'input est vide
      if(this.value.length === 0 && pageContent.style.display === "none") {
        pageContent.style.display = "block";
        divResults.style.display = "none";  
      }
      
      //exclusion des touches hors lettres et chiffres
      if ((this.value.length > 1) &&
      (e.keyCode == 32 ||
        (e.keyCode > 64 && e.keyCode < 91) ||
        (e.keyCode > 96 && e.keyCode < 123) ||
        (e.keyCode == 8 && search.value !== ""))
        ) {
          
          
          fetch(`${baseUrl}/api-search/` + this.value)
          .then((reponse) => reponse.json())
          .then((array) => {
            //remets la page en haut 
            //vide le contenu du résultat a chaque nouvelle recherche
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
            divResults.style.display = "block";  
            wrapper.innerHTML = "";
            section.innerHTML = ""; 
            divResults.removeAttribute("hidden"); 
            pageContent.style.display = "none"; 
            if(array.length === 0){
              const pResult = document.createElement("p");
              pResult.innerHTML = "Aucun résultat :("; 
              wrapper.append(pResult); 
              divResults.append(wrapper); 
            }
            else {
              let aElem; 
              let imageElem; 
              let contentElem; 

              for (const elem of array) {
                const card = document.createElement('div'); 
                card.classList.add('vertical-cards'); 
                //recuperation des valeurs de chasses si propriete hunt et d'actu si propriete news
                if (elem.hasOwnProperty("hunt")) {
                  aElem = document.createElement('a'); 
                  aElem.href = `/hunt/${elem["hunt"].id}`; 
                  aElem.text = elem["hunt"].name; 
                  imageElem = new Image();
                  imageElem.src = elem["hunt"].image; 
                  contentElem = document.createElement("p"); 
                  contentElem = splitText(elem["hunt"].description, 180);
                }
                if (elem.hasOwnProperty("news")) {
                  aElem = document.createElement('a'); 
                  aElem.href = `/news/${elem["news"].id}`; 
                  aElem.text = elem["news"].title; 
                  imageElem = new Image();
                  imageElem.src = elem["news"].image; 
                  contentElem = document.createElement("p"); 
                  contentElem.innerHTML = splitText(elem["news"].content, 180); 
                }

                //création des élements cards et ajout dans le DOM
                const imgContainer = document.createElement('div'); 
                imgContainer.classList.add('img-container');  
                imgContainer.append(imageElem); 
                
                const summary = document.createElement('div'); 
                summary.classList.add('vertical-cards__summary'); 
                
                const h4 = document.createElement('h4'); 
                h4.append(aElem); 

                const description = document.createElement('div'); 
                description.classList.add('vertical-cards__description'); 
                description.append(contentElem); 

                summary.append(h4, description); 

                card.append(imgContainer, summary); 

                section.append(card); 
                wrapper.append(section); 
                divResults.append(wrapper); 
              }
            }
          });
        } else {
          return false;
      }
    });
  });


             