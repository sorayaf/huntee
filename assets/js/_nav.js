document.addEventListener("DOMContentLoaded", () => {

    const openMobMenu = document.querySelector(".open-mobile-menu");
    const closeMobMenu = document.querySelector(".close-mobile-menu");
    const toggleSearchForm = document.querySelector(".search");
    const searchForm = document.querySelector(".page-header form");
    const topMenuWrapper = document.querySelector(".top-menu-wrapper");
    const isVisible = "is-visible";
    const showOffCanvas = "show-offcanvas";

    //Affichage nav et barre de recherche 
    
    openMobMenu.addEventListener("click", () => {
      topMenuWrapper.classList.add(showOffCanvas);
    });
    
    closeMobMenu.addEventListener("click", () => {
      topMenuWrapper.classList.remove(showOffCanvas);
    });
    
    toggleSearchForm.addEventListener("click", () => {
      searchForm.classList.toggle(isVisible);
    });
    
}); 
