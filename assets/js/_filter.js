document.addEventListener("DOMContentLoaded", () => {

  function splitText(text, count){
    const stripedText = text.replace( /(<([^>]+)>)/ig, '');
    return stripedText.slice(0, count) + (text.length > count ? "..." : "");
  }

  const filterDiv = document.querySelector('#filters');  
  
  const baseUrl = 'http://127.0.0.1:8000'; 
  const url= new URL(`${baseUrl}/api/filter/`);

  //fetch du api filter et création de la carte 'hunt' en fonction du res
  function showFilteredHunts()
  {
    const div = document.querySelector('#indexHunts');
    const section = document.createElement('section'); 
    section.classList.add('horizontal-info'); 

    fetch(url)
    .then((response) => {
      return response.json(); 
    })
    .then((res) => {  
        div.innerHTML = "";  
        const result = res; 
        if(result.length === 0)
        {
          const p1 = document.createElement("p"); 
          const p2 = document.createElement("p"); 
          const aNew = document.createElement("a"); 
          div.append('Rien à voir ici pour le moment. 😞', p1);
          aNew.href = '/hunt/new';
          aNew.text = 'Si vous avez connaissance d\'une chasse, vous pouvez la proposer en cliquant ici ! 😊';
          p2.append(aNew); 
          div.append(p2);
        } 
        else
        {

          [...result].forEach(hunt => {
            const card = document.createElement('div'); 
            card.classList.add('horizontal-cards'); 

            const imgContainer = document.createElement('div'); 
            imgContainer.classList.add('img-container'); 
            const img = document.createElement('img');
            img.src = `/uploads/hunts/${hunt['image']}`; 
            imgContainer.append(img); 

            const h3 = document.createElement('h3'); 
            const a = document.createElement('a'); 
            a.href = `/hunt/${hunt['id']}`; 
            a.text = hunt['name']; 
            h3.append(a); 
            
            const divDate = document.createElement('div'); 
            divDate.classList.add('horizontal-cards__date'); 
            const pBeginDate = document.createElement('p'); 
            const pEndDate = document.createElement('p');
            pBeginDate.innerHTML = `Début : ${hunt['beginDate']}`; 
            pEndDate.innerHTML = `Fin : ${hunt['endDate'] ? hunt['endDate'] : 'NC'}`; 
            const divDescr = document.createElement('div'); 
            divDescr.classList.add('horizontal-cards__description'); 
            const pDescr = document.createElement('p'); 
            pDescr.innerHTML = splitText(hunt['description'], 250); 


            divDate.append(pBeginDate, pEndDate); 
            divDescr.append(pDescr); 
            card.append(imgContainer, h3, divDate, divDescr); 
            div.append(section);
            section.append(card); 
          })
        }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  //FILTRE PAR DATE (ajout date comme url param)
  const selectHuntByDate = document.querySelector("#selectHuntByDate");
  selectHuntByDate.addEventListener("change", (e) => {
    e.preventDefault();

    url.searchParams.set('date', `${selectHuntByDate.value}`);
    showFilteredHunts(); 
  });

  //RECHERCHE DPT PAR REGION
  const selectHuntByRegion = document.querySelector('#selectHuntByRegion'); 
  selectHuntByRegion.addEventListener("change", (e) => {
    e.preventDefault();
    const region = selectHuntByRegion.value; 
    fetch(`${baseUrl}/api/filter/regions?region=${region}`)
    .then((response) => {
      return response.json(); 
    })
    .then((res) => {
      if(res === 'FR')
      {
        url.searchParams.delete('dpt'); 
        showFilteredHunts('date', 'descBeginDate'); 
      }
      else
      {
        //recupération des dpts par région 
        //creation du select et des options en gardant le code et le nom du dpt 
        const selectDpt = document.createElement("select");
        selectDpt.setAttribute("id", "selectDpt");
        const optionDisabled = document.createElement("option"); 
        optionDisabled.setAttribute("disabled","disabled");
        optionDisabled.setAttribute("selected","selected");
        optionDisabled.text = "--Sélectionner un département--"; 
        selectDpt.appendChild(optionDisabled); 
        const result = res; 
          [...result].forEach(dpt => {
            const optionDpt = document.createElement("option");
            optionDpt.value = dpt['code'];  
            optionDpt.text = dpt['name'];  
            selectDpt.appendChild(optionDpt); 
          })
        
        filterDiv.append(selectDpt); 
        selectHuntByRegion.addEventListener("change", (e) => {
          e.preventDefault();
          selectDpt.remove(); 
        });
        
        //FILTRE PAR DPT (ajout dpt url param)
        const selectHuntByDpt = document.querySelector('#selectDpt');
        selectHuntByDpt.addEventListener("change", (e) => {
          e.preventDefault(); 
          url.searchParams.set('dpt', `${selectHuntByDpt.value}`);
          showFilteredHunts(); 
        });

        
      }
    })
    .catch((error) => {
      console.log(error);
    });
  });

  //FILTRE PAR CATEGORIE (ajout cat url param)
   const checkboxList = document.querySelectorAll('.checkboxHuntCategory'); 
   const checkedValues = []; 
   for (const checkbox of checkboxList) {
    checkbox.addEventListener("change", (e) => {
      e.preventDefault(); 
      if(e.target.checked)
      {
        checkedValues.push(checkbox.value); 
      } 
      if(e.target.checked === false)
      {
        //retrait de la value du tableau si on retire le check
        const index = checkedValues.indexOf(checkbox.value);
        if(index > -1)
        {
          checkedValues.splice(index, 1);
        }
      }

      url.searchParams.set('cat', `${checkedValues}`);
      showFilteredHunts(); 
    });
  }
});

