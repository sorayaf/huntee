document.addEventListener("DOMContentLoaded", () => {
  //Affichage ou suppression de l'élement du dom en fonction du choix dans le form
  function toggleOption(select, element, value)
  {
    if (select != null) {
      element.style.display = "none";
      select.addEventListener("change", (e) => {
        if (e.target.value === value) {
          element.style.display = "block";
        } else {
          element.style.display = "none";
        }
      });
    }
  }
  const selectIsPayable = document.querySelector("#hunt_isPayable");
  const entryFee = document.querySelector("#huntFormEntryFee");
  toggleOption(selectIsPayable, entryFee, '1'); 

  const selectEndDate = document.querySelector("#selectHuntNewEndDate");
  const endDate = document.querySelector("#huntFormEndDate");
  toggleOption(selectEndDate, endDate, '1'); 

  const selectIsLocated = document.querySelector("#hunt_isLocated");
  const department = document.querySelector("#huntFormDpt");
  toggleOption(selectIsLocated, department, '0'); 

});
