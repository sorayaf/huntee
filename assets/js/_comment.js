document.addEventListener("DOMContentLoaded", () => {
  const editCommentBtns = document.querySelectorAll(".editComment");

  //Création d'un textarea pour chaque clic sur modifier
  for (const editCommentBtn of editCommentBtns) {
    editCommentBtn.addEventListener("click", (e) => {
      const id = e.target.dataset.id;
      const input = document.createElement('textarea'); 
      const parentElem = editCommentBtn.parentNode; 
      const elemMessage = parentElem.children[1].children[0]; 
      const oldMessage = elemMessage.innerHTML;
      const saveBtn = document.createElement('button'); 
      saveBtn.innerHTML = 'Enregistrer';
      
      parentElem.append(input); 
      parentElem.append(saveBtn); 
      editCommentBtn.style.display = "none"; 
        // Affichage de l'ancien message dans l'input
      input.value = oldMessage; 

      //Modification du message au save
      saveBtn.addEventListener("click", () => {
        const message = input.value; 
        fetch(`api/comment/edit/${id}/${message}`)
        .then((response) => {
          return response.json()
        })
        .then((json) => {
            elemMessage.innerHTML=json; 
            input.remove(); 
            saveBtn.remove(); 
            editCommentBtn.style.display = "block"; 
        })
        .catch((error) => {
          console.log(error);
        });
      });
    });
  }
});
