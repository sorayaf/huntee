(self["webpackChunk"] = self["webpackChunk"] || []).push([["app"],{

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_app_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles/app.scss */ "./assets/styles/app.scss");
/* harmony import */ var _js_search_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/_search.js */ "./assets/js/_search.js");
/* harmony import */ var _js_search_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_search_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _js_nav_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/_nav.js */ "./assets/js/_nav.js");
/* harmony import */ var _js_nav_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_js_nav_js__WEBPACK_IMPORTED_MODULE_2__);
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)




/***/ }),

/***/ "./assets/js/_nav.js":
/*!***************************!*\
  !*** ./assets/js/_nav.js ***!
  \***************************/
/***/ (() => {

document.addEventListener("DOMContentLoaded", function () {
  var openMobMenu = document.querySelector(".open-mobile-menu");
  var closeMobMenu = document.querySelector(".close-mobile-menu");
  var toggleSearchForm = document.querySelector(".search");
  var searchForm = document.querySelector(".page-header form");
  var topMenuWrapper = document.querySelector(".top-menu-wrapper");
  var isVisible = "is-visible";
  var showOffCanvas = "show-offcanvas"; //Affichage nav et barre de recherche 

  openMobMenu.addEventListener("click", function () {
    topMenuWrapper.classList.add(showOffCanvas);
  });
  closeMobMenu.addEventListener("click", function () {
    topMenuWrapper.classList.remove(showOffCanvas);
  });
  toggleSearchForm.addEventListener("click", function () {
    searchForm.classList.toggle(isVisible);
  });
});

/***/ }),

/***/ "./assets/js/_search.js":
/*!******************************!*\
  !*** ./assets/js/_search.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

__webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.array.from.js */ "./node_modules/core-js/modules/es.array.from.js");

__webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");

document.addEventListener("DOMContentLoaded", function () {
  function splitText(text, count) {
    var stripedText = text.replace(/(<([^>]+)>)/ig, '');
    return stripedText.slice(0, count) + (text.length > count ? "..." : "");
  }

  var baseUrl = 'http://127.0.0.1:8000';
  var search = document.querySelector("#searchInput");
  var divResults = document.querySelector(".show-results");
  var pageContent = document.querySelector(".page-content");
  var wrapper = document.createElement("div");
  wrapper.classList.add("wrapper");
  var section = document.createElement('section');
  section.classList.add('vertical-info');
  search.addEventListener("keyup", function (e) {
    e.preventDefault(); //affichage du contenu de base si l'input est vide

    if (this.value.length === 0 && pageContent.style.display === "none") {
      pageContent.style.display = "block";
      divResults.style.display = "none";
    } //exclusion des touches hors lettres et chiffres


    if (this.value.length > 1 && (e.keyCode == 32 || e.keyCode > 64 && e.keyCode < 91 || e.keyCode > 96 && e.keyCode < 123 || e.keyCode == 8 && search.value !== "")) {
      fetch("".concat(baseUrl, "/api-search/") + this.value).then(function (reponse) {
        return reponse.json();
      }).then(function (array) {
        //remets la page en haut 
        //vide le contenu du résultat a chaque nouvelle recherche
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        divResults.style.display = "block";
        wrapper.innerHTML = "";
        section.innerHTML = "";
        divResults.removeAttribute("hidden");
        pageContent.style.display = "none";

        if (array.length === 0) {
          var pResult = document.createElement("p");
          pResult.innerHTML = "Aucun résultat :(";
          wrapper.append(pResult);
          divResults.append(wrapper);
        } else {
          var aElem;
          var imageElem;
          var contentElem;

          var _iterator = _createForOfIteratorHelper(array),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var elem = _step.value;
              var card = document.createElement('div');
              card.classList.add('vertical-cards'); //recuperation des valeurs de chasses si propriete hunt et d'actu si propriete news

              if (elem.hasOwnProperty("hunt")) {
                aElem = document.createElement('a');
                aElem.href = "/hunt/".concat(elem["hunt"].id);
                aElem.text = elem["hunt"].name;
                imageElem = new Image();
                imageElem.src = elem["hunt"].image;
                contentElem = document.createElement("p");
                contentElem = splitText(elem["hunt"].description, 180);
              }

              if (elem.hasOwnProperty("news")) {
                aElem = document.createElement('a');
                aElem.href = "/news/".concat(elem["news"].id);
                aElem.text = elem["news"].title;
                imageElem = new Image();
                imageElem.src = elem["news"].image;
                contentElem = document.createElement("p");
                contentElem.innerHTML = splitText(elem["news"].content, 180);
              } //création des élements cards et ajout dans le DOM


              var imgContainer = document.createElement('div');
              imgContainer.classList.add('img-container');
              imgContainer.append(imageElem);
              var summary = document.createElement('div');
              summary.classList.add('vertical-cards__summary');
              var h4 = document.createElement('h4');
              h4.append(aElem);
              var description = document.createElement('div');
              description.classList.add('vertical-cards__description');
              description.append(contentElem);
              summary.append(h4, description);
              card.append(imgContainer, summary);
              section.append(card);
              wrapper.append(section);
              divResults.append(wrapper);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      });
    } else {
      return false;
    }
  });
});

/***/ }),

/***/ "./assets/styles/app.scss":
/*!********************************!*\
  !*** ./assets/styles/app.scss ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_core-js_modules_es_array_from_js-node_modules_core-js_modules_es_array_i-0f4e2e","vendors-node_modules_core-js_modules_es_string_replace_js"], () => (__webpack_exec__("./assets/app.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNUQUEsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBTTtFQUVoRCxJQUFNQyxXQUFXLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixtQkFBdkIsQ0FBcEI7RUFDQSxJQUFNQyxZQUFZLEdBQUdKLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsQ0FBckI7RUFDQSxJQUFNRSxnQkFBZ0IsR0FBR0wsUUFBUSxDQUFDRyxhQUFULENBQXVCLFNBQXZCLENBQXpCO0VBQ0EsSUFBTUcsVUFBVSxHQUFHTixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsbUJBQXZCLENBQW5CO0VBQ0EsSUFBTUksY0FBYyxHQUFHUCxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsbUJBQXZCLENBQXZCO0VBQ0EsSUFBTUssU0FBUyxHQUFHLFlBQWxCO0VBQ0EsSUFBTUMsYUFBYSxHQUFHLGdCQUF0QixDQVJnRCxDQVVoRDs7RUFFQVAsV0FBVyxDQUFDRCxnQkFBWixDQUE2QixPQUE3QixFQUFzQyxZQUFNO0lBQzFDTSxjQUFjLENBQUNHLFNBQWYsQ0FBeUJDLEdBQXpCLENBQTZCRixhQUE3QjtFQUNELENBRkQ7RUFJQUwsWUFBWSxDQUFDSCxnQkFBYixDQUE4QixPQUE5QixFQUF1QyxZQUFNO0lBQzNDTSxjQUFjLENBQUNHLFNBQWYsQ0FBeUJFLE1BQXpCLENBQWdDSCxhQUFoQztFQUNELENBRkQ7RUFJQUosZ0JBQWdCLENBQUNKLGdCQUFqQixDQUFrQyxPQUFsQyxFQUEyQyxZQUFNO0lBQy9DSyxVQUFVLENBQUNJLFNBQVgsQ0FBcUJHLE1BQXJCLENBQTRCTCxTQUE1QjtFQUNELENBRkQ7QUFJSCxDQXhCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQVIsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBTTtFQUdsRCxTQUFTYSxTQUFULENBQW1CQyxJQUFuQixFQUF5QkMsS0FBekIsRUFBK0I7SUFDN0IsSUFBTUMsV0FBVyxHQUFHRixJQUFJLENBQUNHLE9BQUwsQ0FBYyxlQUFkLEVBQStCLEVBQS9CLENBQXBCO0lBQ0EsT0FBT0QsV0FBVyxDQUFDRSxLQUFaLENBQWtCLENBQWxCLEVBQXFCSCxLQUFyQixLQUErQkQsSUFBSSxDQUFDSyxNQUFMLEdBQWNKLEtBQWQsR0FBc0IsS0FBdEIsR0FBOEIsRUFBN0QsQ0FBUDtFQUNEOztFQUVDLElBQU1LLE9BQU8sR0FBRyx1QkFBaEI7RUFDQSxJQUFNQyxNQUFNLEdBQUd0QixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBZjtFQUNBLElBQU1vQixVQUFVLEdBQUd2QixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsZUFBdkIsQ0FBbkI7RUFDQSxJQUFNcUIsV0FBVyxHQUFHeEIsUUFBUSxDQUFDRyxhQUFULENBQXVCLGVBQXZCLENBQXBCO0VBQ0EsSUFBTXNCLE9BQU8sR0FBR3pCLFFBQVEsQ0FBQzBCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBaEI7RUFDQUQsT0FBTyxDQUFDZixTQUFSLENBQWtCQyxHQUFsQixDQUFzQixTQUF0QjtFQUNBLElBQU1nQixPQUFPLEdBQUczQixRQUFRLENBQUMwQixhQUFULENBQXVCLFNBQXZCLENBQWhCO0VBQ0FDLE9BQU8sQ0FBQ2pCLFNBQVIsQ0FBa0JDLEdBQWxCLENBQXNCLGVBQXRCO0VBRUFXLE1BQU0sQ0FBQ3JCLGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLFVBQVUyQixDQUFWLEVBQWE7SUFDNUNBLENBQUMsQ0FBQ0MsY0FBRixHQUQ0QyxDQUc1Qzs7SUFDQSxJQUFHLEtBQUtDLEtBQUwsQ0FBV1YsTUFBWCxLQUFzQixDQUF0QixJQUEyQkksV0FBVyxDQUFDTyxLQUFaLENBQWtCQyxPQUFsQixLQUE4QixNQUE1RCxFQUFvRTtNQUNsRVIsV0FBVyxDQUFDTyxLQUFaLENBQWtCQyxPQUFsQixHQUE0QixPQUE1QjtNQUNBVCxVQUFVLENBQUNRLEtBQVgsQ0FBaUJDLE9BQWpCLEdBQTJCLE1BQTNCO0lBQ0QsQ0FQMkMsQ0FTNUM7OztJQUNBLElBQUssS0FBS0YsS0FBTCxDQUFXVixNQUFYLEdBQW9CLENBQXJCLEtBQ0hRLENBQUMsQ0FBQ0ssT0FBRixJQUFhLEVBQWIsSUFDRUwsQ0FBQyxDQUFDSyxPQUFGLEdBQVksRUFBWixJQUFrQkwsQ0FBQyxDQUFDSyxPQUFGLEdBQVksRUFEaEMsSUFFRUwsQ0FBQyxDQUFDSyxPQUFGLEdBQVksRUFBWixJQUFrQkwsQ0FBQyxDQUFDSyxPQUFGLEdBQVksR0FGaEMsSUFHRUwsQ0FBQyxDQUFDSyxPQUFGLElBQWEsQ0FBYixJQUFrQlgsTUFBTSxDQUFDUSxLQUFQLEtBQWlCLEVBSmxDLENBQUosRUFLSTtNQUdBSSxLQUFLLENBQUMsVUFBR2IsT0FBSCxvQkFBMkIsS0FBS1MsS0FBakMsQ0FBTCxDQUNDSyxJQURELENBQ00sVUFBQ0MsT0FBRDtRQUFBLE9BQWFBLE9BQU8sQ0FBQ0MsSUFBUixFQUFiO01BQUEsQ0FETixFQUVDRixJQUZELENBRU0sVUFBQ0csS0FBRCxFQUFXO1FBQ2Y7UUFDQTtRQUNBdEMsUUFBUSxDQUFDdUMsSUFBVCxDQUFjQyxTQUFkLEdBQTBCLENBQTFCO1FBQ0F4QyxRQUFRLENBQUN5QyxlQUFULENBQXlCRCxTQUF6QixHQUFxQyxDQUFyQztRQUNBakIsVUFBVSxDQUFDUSxLQUFYLENBQWlCQyxPQUFqQixHQUEyQixPQUEzQjtRQUNBUCxPQUFPLENBQUNpQixTQUFSLEdBQW9CLEVBQXBCO1FBQ0FmLE9BQU8sQ0FBQ2UsU0FBUixHQUFvQixFQUFwQjtRQUNBbkIsVUFBVSxDQUFDb0IsZUFBWCxDQUEyQixRQUEzQjtRQUNBbkIsV0FBVyxDQUFDTyxLQUFaLENBQWtCQyxPQUFsQixHQUE0QixNQUE1Qjs7UUFDQSxJQUFHTSxLQUFLLENBQUNsQixNQUFOLEtBQWlCLENBQXBCLEVBQXNCO1VBQ3BCLElBQU13QixPQUFPLEdBQUc1QyxRQUFRLENBQUMwQixhQUFULENBQXVCLEdBQXZCLENBQWhCO1VBQ0FrQixPQUFPLENBQUNGLFNBQVIsR0FBb0IsbUJBQXBCO1VBQ0FqQixPQUFPLENBQUNvQixNQUFSLENBQWVELE9BQWY7VUFDQXJCLFVBQVUsQ0FBQ3NCLE1BQVgsQ0FBa0JwQixPQUFsQjtRQUNELENBTEQsTUFNSztVQUNILElBQUlxQixLQUFKO1VBQ0EsSUFBSUMsU0FBSjtVQUNBLElBQUlDLFdBQUo7O1VBSEcsMkNBS2dCVixLQUxoQjtVQUFBOztVQUFBO1lBS0gsb0RBQTBCO2NBQUEsSUFBZlcsSUFBZTtjQUN4QixJQUFNQyxJQUFJLEdBQUdsRCxRQUFRLENBQUMwQixhQUFULENBQXVCLEtBQXZCLENBQWI7Y0FDQXdCLElBQUksQ0FBQ3hDLFNBQUwsQ0FBZUMsR0FBZixDQUFtQixnQkFBbkIsRUFGd0IsQ0FHeEI7O2NBQ0EsSUFBSXNDLElBQUksQ0FBQ0UsY0FBTCxDQUFvQixNQUFwQixDQUFKLEVBQWlDO2dCQUMvQkwsS0FBSyxHQUFHOUMsUUFBUSxDQUFDMEIsYUFBVCxDQUF1QixHQUF2QixDQUFSO2dCQUNBb0IsS0FBSyxDQUFDTSxJQUFOLG1CQUFzQkgsSUFBSSxDQUFDLE1BQUQsQ0FBSixDQUFhSSxFQUFuQztnQkFDQVAsS0FBSyxDQUFDL0IsSUFBTixHQUFha0MsSUFBSSxDQUFDLE1BQUQsQ0FBSixDQUFhSyxJQUExQjtnQkFDQVAsU0FBUyxHQUFHLElBQUlRLEtBQUosRUFBWjtnQkFDQVIsU0FBUyxDQUFDUyxHQUFWLEdBQWdCUCxJQUFJLENBQUMsTUFBRCxDQUFKLENBQWFRLEtBQTdCO2dCQUNBVCxXQUFXLEdBQUdoRCxRQUFRLENBQUMwQixhQUFULENBQXVCLEdBQXZCLENBQWQ7Z0JBQ0FzQixXQUFXLEdBQUdsQyxTQUFTLENBQUNtQyxJQUFJLENBQUMsTUFBRCxDQUFKLENBQWFTLFdBQWQsRUFBMkIsR0FBM0IsQ0FBdkI7Y0FDRDs7Y0FDRCxJQUFJVCxJQUFJLENBQUNFLGNBQUwsQ0FBb0IsTUFBcEIsQ0FBSixFQUFpQztnQkFDL0JMLEtBQUssR0FBRzlDLFFBQVEsQ0FBQzBCLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBUjtnQkFDQW9CLEtBQUssQ0FBQ00sSUFBTixtQkFBc0JILElBQUksQ0FBQyxNQUFELENBQUosQ0FBYUksRUFBbkM7Z0JBQ0FQLEtBQUssQ0FBQy9CLElBQU4sR0FBYWtDLElBQUksQ0FBQyxNQUFELENBQUosQ0FBYVUsS0FBMUI7Z0JBQ0FaLFNBQVMsR0FBRyxJQUFJUSxLQUFKLEVBQVo7Z0JBQ0FSLFNBQVMsQ0FBQ1MsR0FBVixHQUFnQlAsSUFBSSxDQUFDLE1BQUQsQ0FBSixDQUFhUSxLQUE3QjtnQkFDQVQsV0FBVyxHQUFHaEQsUUFBUSxDQUFDMEIsYUFBVCxDQUF1QixHQUF2QixDQUFkO2dCQUNBc0IsV0FBVyxDQUFDTixTQUFaLEdBQXdCNUIsU0FBUyxDQUFDbUMsSUFBSSxDQUFDLE1BQUQsQ0FBSixDQUFhVyxPQUFkLEVBQXVCLEdBQXZCLENBQWpDO2NBQ0QsQ0FyQnVCLENBdUJ4Qjs7O2NBQ0EsSUFBTUMsWUFBWSxHQUFHN0QsUUFBUSxDQUFDMEIsYUFBVCxDQUF1QixLQUF2QixDQUFyQjtjQUNBbUMsWUFBWSxDQUFDbkQsU0FBYixDQUF1QkMsR0FBdkIsQ0FBMkIsZUFBM0I7Y0FDQWtELFlBQVksQ0FBQ2hCLE1BQWIsQ0FBb0JFLFNBQXBCO2NBRUEsSUFBTWUsT0FBTyxHQUFHOUQsUUFBUSxDQUFDMEIsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtjQUNBb0MsT0FBTyxDQUFDcEQsU0FBUixDQUFrQkMsR0FBbEIsQ0FBc0IseUJBQXRCO2NBRUEsSUFBTW9ELEVBQUUsR0FBRy9ELFFBQVEsQ0FBQzBCLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBWDtjQUNBcUMsRUFBRSxDQUFDbEIsTUFBSCxDQUFVQyxLQUFWO2NBRUEsSUFBTVksV0FBVyxHQUFHMUQsUUFBUSxDQUFDMEIsYUFBVCxDQUF1QixLQUF2QixDQUFwQjtjQUNBZ0MsV0FBVyxDQUFDaEQsU0FBWixDQUFzQkMsR0FBdEIsQ0FBMEIsNkJBQTFCO2NBQ0ErQyxXQUFXLENBQUNiLE1BQVosQ0FBbUJHLFdBQW5CO2NBRUFjLE9BQU8sQ0FBQ2pCLE1BQVIsQ0FBZWtCLEVBQWYsRUFBbUJMLFdBQW5CO2NBRUFSLElBQUksQ0FBQ0wsTUFBTCxDQUFZZ0IsWUFBWixFQUEwQkMsT0FBMUI7Y0FFQW5DLE9BQU8sQ0FBQ2tCLE1BQVIsQ0FBZUssSUFBZjtjQUNBekIsT0FBTyxDQUFDb0IsTUFBUixDQUFlbEIsT0FBZjtjQUNBSixVQUFVLENBQUNzQixNQUFYLENBQWtCcEIsT0FBbEI7WUFDRDtVQWxERTtZQUFBO1VBQUE7WUFBQTtVQUFBO1FBbURKO01BQ0YsQ0F0RUQ7SUF1RUQsQ0EvRUgsTUErRVM7TUFDTCxPQUFPLEtBQVA7SUFDSDtFQUNGLENBNUZEO0FBNkZELENBOUdIOzs7Ozs7Ozs7Ozs7QUNBQSIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2Fzc2V0cy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL19uYXYuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL19zZWFyY2guanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3N0eWxlcy9hcHAuc2Nzcz84ZjU5Il0sInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBXZWxjb21lIHRvIHlvdXIgYXBwJ3MgbWFpbiBKYXZhU2NyaXB0IGZpbGUhXG4gKlxuICogV2UgcmVjb21tZW5kIGluY2x1ZGluZyB0aGUgYnVpbHQgdmVyc2lvbiBvZiB0aGlzIEphdmFTY3JpcHQgZmlsZVxuICogKGFuZCBpdHMgQ1NTIGZpbGUpIGluIHlvdXIgYmFzZSBsYXlvdXQgKGJhc2UuaHRtbC50d2lnKS5cbiAqL1xuXG4vLyBhbnkgQ1NTIHlvdSBpbXBvcnQgd2lsbCBvdXRwdXQgaW50byBhIHNpbmdsZSBjc3MgZmlsZSAoYXBwLmNzcyBpbiB0aGlzIGNhc2UpXG5pbXBvcnQgXCIuL3N0eWxlcy9hcHAuc2Nzc1wiO1xuaW1wb3J0ICcuL2pzL19zZWFyY2guanMnO1xuaW1wb3J0ICcuL2pzL19uYXYuanMnO1xuIiwiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgKCkgPT4ge1xuXG4gICAgY29uc3Qgb3Blbk1vYk1lbnUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLm9wZW4tbW9iaWxlLW1lbnVcIik7XG4gICAgY29uc3QgY2xvc2VNb2JNZW51ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5jbG9zZS1tb2JpbGUtbWVudVwiKTtcbiAgICBjb25zdCB0b2dnbGVTZWFyY2hGb3JtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5zZWFyY2hcIik7XG4gICAgY29uc3Qgc2VhcmNoRm9ybSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIucGFnZS1oZWFkZXIgZm9ybVwiKTtcbiAgICBjb25zdCB0b3BNZW51V3JhcHBlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudG9wLW1lbnUtd3JhcHBlclwiKTtcbiAgICBjb25zdCBpc1Zpc2libGUgPSBcImlzLXZpc2libGVcIjtcbiAgICBjb25zdCBzaG93T2ZmQ2FudmFzID0gXCJzaG93LW9mZmNhbnZhc1wiO1xuXG4gICAgLy9BZmZpY2hhZ2UgbmF2IGV0IGJhcnJlIGRlIHJlY2hlcmNoZSBcbiAgICBcbiAgICBvcGVuTW9iTWVudS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4ge1xuICAgICAgdG9wTWVudVdyYXBwZXIuY2xhc3NMaXN0LmFkZChzaG93T2ZmQ2FudmFzKTtcbiAgICB9KTtcbiAgICBcbiAgICBjbG9zZU1vYk1lbnUuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHtcbiAgICAgIHRvcE1lbnVXcmFwcGVyLmNsYXNzTGlzdC5yZW1vdmUoc2hvd09mZkNhbnZhcyk7XG4gICAgfSk7XG4gICAgXG4gICAgdG9nZ2xlU2VhcmNoRm9ybS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4ge1xuICAgICAgc2VhcmNoRm9ybS5jbGFzc0xpc3QudG9nZ2xlKGlzVmlzaWJsZSk7XG4gICAgfSk7XG4gICAgXG59KTsgXG4iLCJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCAoKSA9PiB7XG4gICAgICAgXG5cbiAgZnVuY3Rpb24gc3BsaXRUZXh0KHRleHQsIGNvdW50KXtcbiAgICBjb25zdCBzdHJpcGVkVGV4dCA9IHRleHQucmVwbGFjZSggLyg8KFtePl0rKT4pL2lnLCAnJyk7XG4gICAgcmV0dXJuIHN0cmlwZWRUZXh0LnNsaWNlKDAsIGNvdW50KSArICh0ZXh0Lmxlbmd0aCA+IGNvdW50ID8gXCIuLi5cIiA6IFwiXCIpO1xuICB9XG5cbiAgICBjb25zdCBiYXNlVXJsID0gJ2h0dHA6Ly8xMjcuMC4wLjE6ODAwMCc7IFxuICAgIGNvbnN0IHNlYXJjaCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2VhcmNoSW5wdXRcIik7XG4gICAgY29uc3QgZGl2UmVzdWx0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuc2hvdy1yZXN1bHRzXCIpO1xuICAgIGNvbnN0IHBhZ2VDb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5wYWdlLWNvbnRlbnRcIik7IFxuICAgIGNvbnN0IHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIHdyYXBwZXIuY2xhc3NMaXN0LmFkZChcIndyYXBwZXJcIik7IFxuICAgIGNvbnN0IHNlY3Rpb24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzZWN0aW9uJyk7IFxuICAgIHNlY3Rpb24uY2xhc3NMaXN0LmFkZCgndmVydGljYWwtaW5mbycpOyBcblxuICAgIHNlYXJjaC5hZGRFdmVudExpc3RlbmVyKFwia2V5dXBcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIFxuICAgICAgLy9hZmZpY2hhZ2UgZHUgY29udGVudSBkZSBiYXNlIHNpIGwnaW5wdXQgZXN0IHZpZGVcbiAgICAgIGlmKHRoaXMudmFsdWUubGVuZ3RoID09PSAwICYmIHBhZ2VDb250ZW50LnN0eWxlLmRpc3BsYXkgPT09IFwibm9uZVwiKSB7XG4gICAgICAgIHBhZ2VDb250ZW50LnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgICAgIGRpdlJlc3VsdHMuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiOyAgXG4gICAgICB9XG4gICAgICBcbiAgICAgIC8vZXhjbHVzaW9uIGRlcyB0b3VjaGVzIGhvcnMgbGV0dHJlcyBldCBjaGlmZnJlc1xuICAgICAgaWYgKCh0aGlzLnZhbHVlLmxlbmd0aCA+IDEpICYmXG4gICAgICAoZS5rZXlDb2RlID09IDMyIHx8XG4gICAgICAgIChlLmtleUNvZGUgPiA2NCAmJiBlLmtleUNvZGUgPCA5MSkgfHxcbiAgICAgICAgKGUua2V5Q29kZSA+IDk2ICYmIGUua2V5Q29kZSA8IDEyMykgfHxcbiAgICAgICAgKGUua2V5Q29kZSA9PSA4ICYmIHNlYXJjaC52YWx1ZSAhPT0gXCJcIikpXG4gICAgICAgICkge1xuICAgICAgICAgIFxuICAgICAgICAgIFxuICAgICAgICAgIGZldGNoKGAke2Jhc2VVcmx9L2FwaS1zZWFyY2gvYCArIHRoaXMudmFsdWUpXG4gICAgICAgICAgLnRoZW4oKHJlcG9uc2UpID0+IHJlcG9uc2UuanNvbigpKVxuICAgICAgICAgIC50aGVuKChhcnJheSkgPT4ge1xuICAgICAgICAgICAgLy9yZW1ldHMgbGEgcGFnZSBlbiBoYXV0IFxuICAgICAgICAgICAgLy92aWRlIGxlIGNvbnRlbnUgZHUgcsOpc3VsdGF0IGEgY2hhcXVlIG5vdXZlbGxlIHJlY2hlcmNoZVxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwO1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCA9IDA7XG4gICAgICAgICAgICBkaXZSZXN1bHRzLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7ICBcbiAgICAgICAgICAgIHdyYXBwZXIuaW5uZXJIVE1MID0gXCJcIjtcbiAgICAgICAgICAgIHNlY3Rpb24uaW5uZXJIVE1MID0gXCJcIjsgXG4gICAgICAgICAgICBkaXZSZXN1bHRzLnJlbW92ZUF0dHJpYnV0ZShcImhpZGRlblwiKTsgXG4gICAgICAgICAgICBwYWdlQ29udGVudC5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7IFxuICAgICAgICAgICAgaWYoYXJyYXkubGVuZ3RoID09PSAwKXtcbiAgICAgICAgICAgICAgY29uc3QgcFJlc3VsdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwXCIpO1xuICAgICAgICAgICAgICBwUmVzdWx0LmlubmVySFRNTCA9IFwiQXVjdW4gcsOpc3VsdGF0IDooXCI7IFxuICAgICAgICAgICAgICB3cmFwcGVyLmFwcGVuZChwUmVzdWx0KTsgXG4gICAgICAgICAgICAgIGRpdlJlc3VsdHMuYXBwZW5kKHdyYXBwZXIpOyBcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBsZXQgYUVsZW07IFxuICAgICAgICAgICAgICBsZXQgaW1hZ2VFbGVtOyBcbiAgICAgICAgICAgICAgbGV0IGNvbnRlbnRFbGVtOyBcblxuICAgICAgICAgICAgICBmb3IgKGNvbnN0IGVsZW0gb2YgYXJyYXkpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjYXJkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7IFxuICAgICAgICAgICAgICAgIGNhcmQuY2xhc3NMaXN0LmFkZCgndmVydGljYWwtY2FyZHMnKTsgXG4gICAgICAgICAgICAgICAgLy9yZWN1cGVyYXRpb24gZGVzIHZhbGV1cnMgZGUgY2hhc3NlcyBzaSBwcm9wcmlldGUgaHVudCBldCBkJ2FjdHUgc2kgcHJvcHJpZXRlIG5ld3NcbiAgICAgICAgICAgICAgICBpZiAoZWxlbS5oYXNPd25Qcm9wZXJ0eShcImh1bnRcIikpIHtcbiAgICAgICAgICAgICAgICAgIGFFbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpOyBcbiAgICAgICAgICAgICAgICAgIGFFbGVtLmhyZWYgPSBgL2h1bnQvJHtlbGVtW1wiaHVudFwiXS5pZH1gOyBcbiAgICAgICAgICAgICAgICAgIGFFbGVtLnRleHQgPSBlbGVtW1wiaHVudFwiXS5uYW1lOyBcbiAgICAgICAgICAgICAgICAgIGltYWdlRWxlbSA9IG5ldyBJbWFnZSgpO1xuICAgICAgICAgICAgICAgICAgaW1hZ2VFbGVtLnNyYyA9IGVsZW1bXCJodW50XCJdLmltYWdlOyBcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRFbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7IFxuICAgICAgICAgICAgICAgICAgY29udGVudEVsZW0gPSBzcGxpdFRleHQoZWxlbVtcImh1bnRcIl0uZGVzY3JpcHRpb24sIDE4MCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChlbGVtLmhhc093blByb3BlcnR5KFwibmV3c1wiKSkge1xuICAgICAgICAgICAgICAgICAgYUVsZW0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7IFxuICAgICAgICAgICAgICAgICAgYUVsZW0uaHJlZiA9IGAvbmV3cy8ke2VsZW1bXCJuZXdzXCJdLmlkfWA7IFxuICAgICAgICAgICAgICAgICAgYUVsZW0udGV4dCA9IGVsZW1bXCJuZXdzXCJdLnRpdGxlOyBcbiAgICAgICAgICAgICAgICAgIGltYWdlRWxlbSA9IG5ldyBJbWFnZSgpO1xuICAgICAgICAgICAgICAgICAgaW1hZ2VFbGVtLnNyYyA9IGVsZW1bXCJuZXdzXCJdLmltYWdlOyBcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRFbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInBcIik7IFxuICAgICAgICAgICAgICAgICAgY29udGVudEVsZW0uaW5uZXJIVE1MID0gc3BsaXRUZXh0KGVsZW1bXCJuZXdzXCJdLmNvbnRlbnQsIDE4MCk7IFxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vY3LDqWF0aW9uIGRlcyDDqWxlbWVudHMgY2FyZHMgZXQgYWpvdXQgZGFucyBsZSBET01cbiAgICAgICAgICAgICAgICBjb25zdCBpbWdDb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTsgXG4gICAgICAgICAgICAgICAgaW1nQ29udGFpbmVyLmNsYXNzTGlzdC5hZGQoJ2ltZy1jb250YWluZXInKTsgIFxuICAgICAgICAgICAgICAgIGltZ0NvbnRhaW5lci5hcHBlbmQoaW1hZ2VFbGVtKTsgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY29uc3Qgc3VtbWFyeSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpOyBcbiAgICAgICAgICAgICAgICBzdW1tYXJ5LmNsYXNzTGlzdC5hZGQoJ3ZlcnRpY2FsLWNhcmRzX19zdW1tYXJ5Jyk7IFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGNvbnN0IGg0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaDQnKTsgXG4gICAgICAgICAgICAgICAgaDQuYXBwZW5kKGFFbGVtKTsgXG5cbiAgICAgICAgICAgICAgICBjb25zdCBkZXNjcmlwdGlvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpOyBcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbi5jbGFzc0xpc3QuYWRkKCd2ZXJ0aWNhbC1jYXJkc19fZGVzY3JpcHRpb24nKTsgXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb24uYXBwZW5kKGNvbnRlbnRFbGVtKTsgXG5cbiAgICAgICAgICAgICAgICBzdW1tYXJ5LmFwcGVuZChoNCwgZGVzY3JpcHRpb24pOyBcblxuICAgICAgICAgICAgICAgIGNhcmQuYXBwZW5kKGltZ0NvbnRhaW5lciwgc3VtbWFyeSk7IFxuXG4gICAgICAgICAgICAgICAgc2VjdGlvbi5hcHBlbmQoY2FyZCk7IFxuICAgICAgICAgICAgICAgIHdyYXBwZXIuYXBwZW5kKHNlY3Rpb24pOyBcbiAgICAgICAgICAgICAgICBkaXZSZXN1bHRzLmFwcGVuZCh3cmFwcGVyKTsgXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuXG5cbiAgICAgICAgICAgICAiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW5cbmV4cG9ydCB7fTsiXSwibmFtZXMiOlsiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwib3Blbk1vYk1lbnUiLCJxdWVyeVNlbGVjdG9yIiwiY2xvc2VNb2JNZW51IiwidG9nZ2xlU2VhcmNoRm9ybSIsInNlYXJjaEZvcm0iLCJ0b3BNZW51V3JhcHBlciIsImlzVmlzaWJsZSIsInNob3dPZmZDYW52YXMiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJ0b2dnbGUiLCJzcGxpdFRleHQiLCJ0ZXh0IiwiY291bnQiLCJzdHJpcGVkVGV4dCIsInJlcGxhY2UiLCJzbGljZSIsImxlbmd0aCIsImJhc2VVcmwiLCJzZWFyY2giLCJkaXZSZXN1bHRzIiwicGFnZUNvbnRlbnQiLCJ3cmFwcGVyIiwiY3JlYXRlRWxlbWVudCIsInNlY3Rpb24iLCJlIiwicHJldmVudERlZmF1bHQiLCJ2YWx1ZSIsInN0eWxlIiwiZGlzcGxheSIsImtleUNvZGUiLCJmZXRjaCIsInRoZW4iLCJyZXBvbnNlIiwianNvbiIsImFycmF5IiwiYm9keSIsInNjcm9sbFRvcCIsImRvY3VtZW50RWxlbWVudCIsImlubmVySFRNTCIsInJlbW92ZUF0dHJpYnV0ZSIsInBSZXN1bHQiLCJhcHBlbmQiLCJhRWxlbSIsImltYWdlRWxlbSIsImNvbnRlbnRFbGVtIiwiZWxlbSIsImNhcmQiLCJoYXNPd25Qcm9wZXJ0eSIsImhyZWYiLCJpZCIsIm5hbWUiLCJJbWFnZSIsInNyYyIsImltYWdlIiwiZGVzY3JpcHRpb24iLCJ0aXRsZSIsImNvbnRlbnQiLCJpbWdDb250YWluZXIiLCJzdW1tYXJ5IiwiaDQiXSwic291cmNlUm9vdCI6IiJ9