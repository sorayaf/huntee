(self["webpackChunk"] = self["webpackChunk"] || []).push([["filter"],{

/***/ "./assets/filter.js":
/*!**************************!*\
  !*** ./assets/filter.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _js_filter_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/_filter.js */ "./assets/js/_filter.js");
/* harmony import */ var _js_filter_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_js_filter_js__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./assets/js/_filter.js":
/*!******************************!*\
  !*** ./assets/js/_filter.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

__webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.url.js */ "./node_modules/core-js/modules/web.url.js");

__webpack_require__(/*! core-js/modules/web.url-search-params.js */ "./node_modules/core-js/modules/web.url-search-params.js");

__webpack_require__(/*! core-js/modules/es.promise.js */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.array.splice.js */ "./node_modules/core-js/modules/es.array.splice.js");

__webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.from.js */ "./node_modules/core-js/modules/es.array.from.js");

document.addEventListener("DOMContentLoaded", function () {
  function splitText(text, count) {
    var stripedText = text.replace(/(<([^>]+)>)/ig, '');
    return stripedText.slice(0, count) + (text.length > count ? "..." : "");
  }

  var filterDiv = document.querySelector('#filters');
  var baseUrl = 'http://127.0.0.1:8000';
  var url = new URL("".concat(baseUrl, "/api/filter/")); //fetch du api filter et création de la carte 'hunt' en fonction du res

  function showFilteredHunts() {
    var div = document.querySelector('#indexHunts');
    var section = document.createElement('section');
    section.classList.add('horizontal-info');
    fetch(url).then(function (response) {
      return response.json();
    }).then(function (res) {
      div.innerHTML = "";
      var result = res;

      if (result.length === 0) {
        var p1 = document.createElement("p");
        var p2 = document.createElement("p");
        var aNew = document.createElement("a");
        div.append('Rien à voir ici pour le moment. 😞', p1);
        aNew.href = '/hunt/new';
        aNew.text = 'Si vous avez connaissance d\'une chasse, vous pouvez la proposer en cliquant ici ! 😊';
        p2.append(aNew);
        div.append(p2);
      } else {
        _toConsumableArray(result).forEach(function (hunt) {
          var card = document.createElement('div');
          card.classList.add('horizontal-cards');
          var imgContainer = document.createElement('div');
          imgContainer.classList.add('img-container');
          var img = document.createElement('img');
          img.src = "/uploads/hunts/".concat(hunt['image']);
          imgContainer.append(img);
          var h3 = document.createElement('h3');
          var a = document.createElement('a');
          a.href = "/hunt/".concat(hunt['id']);
          a.text = hunt['name'];
          h3.append(a);
          var divDate = document.createElement('div');
          divDate.classList.add('horizontal-cards__date');
          var pBeginDate = document.createElement('p');
          var pEndDate = document.createElement('p');
          pBeginDate.innerHTML = "D\xE9but : ".concat(hunt['beginDate']);
          pEndDate.innerHTML = "Fin : ".concat(hunt['endDate'] ? hunt['endDate'] : 'NC');
          var divDescr = document.createElement('div');
          divDescr.classList.add('horizontal-cards__description');
          var pDescr = document.createElement('p');
          pDescr.innerHTML = splitText(hunt['description'], 250);
          divDate.append(pBeginDate, pEndDate);
          divDescr.append(pDescr);
          card.append(imgContainer, h3, divDate, divDescr);
          div.append(section);
          section.append(card);
        });
      }
    })["catch"](function (error) {
      console.log(error);
    });
  } //FILTRE PAR DATE (ajout date comme url param)


  var selectHuntByDate = document.querySelector("#selectHuntByDate");
  selectHuntByDate.addEventListener("change", function (e) {
    e.preventDefault();
    url.searchParams.set('date', "".concat(selectHuntByDate.value));
    showFilteredHunts();
  }); //RECHERCHE DPT PAR REGION

  var selectHuntByRegion = document.querySelector('#selectHuntByRegion');
  selectHuntByRegion.addEventListener("change", function (e) {
    e.preventDefault();
    var region = selectHuntByRegion.value;
    fetch("".concat(baseUrl, "/api/filter/regions?region=").concat(region)).then(function (response) {
      return response.json();
    }).then(function (res) {
      if (res === 'FR') {
        url.searchParams["delete"]('dpt');
        showFilteredHunts('date', 'descBeginDate');
      } else {
        //recupération des dpts par région 
        //creation du select et des options en gardant le code et le nom du dpt 
        var selectDpt = document.createElement("select");
        selectDpt.setAttribute("id", "selectDpt");
        var optionDisabled = document.createElement("option");
        optionDisabled.setAttribute("disabled", "disabled");
        optionDisabled.setAttribute("selected", "selected");
        optionDisabled.text = "--Sélectionner un département--";
        selectDpt.appendChild(optionDisabled);
        var result = res;

        _toConsumableArray(result).forEach(function (dpt) {
          var optionDpt = document.createElement("option");
          optionDpt.value = dpt['code'];
          optionDpt.text = dpt['name'];
          selectDpt.appendChild(optionDpt);
        });

        filterDiv.append(selectDpt);
        selectHuntByRegion.addEventListener("change", function (e) {
          e.preventDefault();
          selectDpt.remove();
        }); //FILTRE PAR DPT (ajout dpt url param)

        var selectHuntByDpt = document.querySelector('#selectDpt');
        selectHuntByDpt.addEventListener("change", function (e) {
          e.preventDefault();
          url.searchParams.set('dpt', "".concat(selectHuntByDpt.value));
          showFilteredHunts();
        });
      }
    })["catch"](function (error) {
      console.log(error);
    });
  }); //FILTRE PAR CATEGORIE (ajout cat url param)

  var checkboxList = document.querySelectorAll('.checkboxHuntCategory');
  var checkedValues = [];

  var _iterator = _createForOfIteratorHelper(checkboxList),
      _step;

  try {
    var _loop = function _loop() {
      var checkbox = _step.value;
      checkbox.addEventListener("change", function (e) {
        e.preventDefault();

        if (e.target.checked) {
          checkedValues.push(checkbox.value);
        }

        if (e.target.checked === false) {
          //retrait de la value du tableau si on retire le check
          var index = checkedValues.indexOf(checkbox.value);

          if (index > -1) {
            checkedValues.splice(index, 1);
          }
        }

        url.searchParams.set('cat', "".concat(checkedValues));
        showFilteredHunts();
      });
    };

    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      _loop();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
});

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_core-js_modules_es_array_from_js-node_modules_core-js_modules_es_array_i-0f4e2e","vendors-node_modules_core-js_modules_es_string_replace_js","vendors-node_modules_core-js_modules_es_array_for-each_js-node_modules_core-js_modules_es_arr-0dba77"], () => (__webpack_exec__("./assets/filter.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBTTtFQUVsRCxTQUFTQyxTQUFULENBQW1CQyxJQUFuQixFQUF5QkMsS0FBekIsRUFBK0I7SUFDN0IsSUFBTUMsV0FBVyxHQUFHRixJQUFJLENBQUNHLE9BQUwsQ0FBYyxlQUFkLEVBQStCLEVBQS9CLENBQXBCO0lBQ0EsT0FBT0QsV0FBVyxDQUFDRSxLQUFaLENBQWtCLENBQWxCLEVBQXFCSCxLQUFyQixLQUErQkQsSUFBSSxDQUFDSyxNQUFMLEdBQWNKLEtBQWQsR0FBc0IsS0FBdEIsR0FBOEIsRUFBN0QsQ0FBUDtFQUNEOztFQUVELElBQU1LLFNBQVMsR0FBR1QsUUFBUSxDQUFDVSxhQUFULENBQXVCLFVBQXZCLENBQWxCO0VBRUEsSUFBTUMsT0FBTyxHQUFHLHVCQUFoQjtFQUNBLElBQU1DLEdBQUcsR0FBRSxJQUFJQyxHQUFKLFdBQVdGLE9BQVgsa0JBQVgsQ0FWa0QsQ0FZbEQ7O0VBQ0EsU0FBU0csaUJBQVQsR0FDQTtJQUNFLElBQU1DLEdBQUcsR0FBR2YsUUFBUSxDQUFDVSxhQUFULENBQXVCLGFBQXZCLENBQVo7SUFDQSxJQUFNTSxPQUFPLEdBQUdoQixRQUFRLENBQUNpQixhQUFULENBQXVCLFNBQXZCLENBQWhCO0lBQ0FELE9BQU8sQ0FBQ0UsU0FBUixDQUFrQkMsR0FBbEIsQ0FBc0IsaUJBQXRCO0lBRUFDLEtBQUssQ0FBQ1IsR0FBRCxDQUFMLENBQ0NTLElBREQsQ0FDTSxVQUFDQyxRQUFELEVBQWM7TUFDbEIsT0FBT0EsUUFBUSxDQUFDQyxJQUFULEVBQVA7SUFDRCxDQUhELEVBSUNGLElBSkQsQ0FJTSxVQUFDRyxHQUFELEVBQVM7TUFDWFQsR0FBRyxDQUFDVSxTQUFKLEdBQWdCLEVBQWhCO01BQ0EsSUFBTUMsTUFBTSxHQUFHRixHQUFmOztNQUNBLElBQUdFLE1BQU0sQ0FBQ2xCLE1BQVAsS0FBa0IsQ0FBckIsRUFDQTtRQUNFLElBQU1tQixFQUFFLEdBQUczQixRQUFRLENBQUNpQixhQUFULENBQXVCLEdBQXZCLENBQVg7UUFDQSxJQUFNVyxFQUFFLEdBQUc1QixRQUFRLENBQUNpQixhQUFULENBQXVCLEdBQXZCLENBQVg7UUFDQSxJQUFNWSxJQUFJLEdBQUc3QixRQUFRLENBQUNpQixhQUFULENBQXVCLEdBQXZCLENBQWI7UUFDQUYsR0FBRyxDQUFDZSxNQUFKLENBQVcsb0NBQVgsRUFBaURILEVBQWpEO1FBQ0FFLElBQUksQ0FBQ0UsSUFBTCxHQUFZLFdBQVo7UUFDQUYsSUFBSSxDQUFDMUIsSUFBTCxHQUFZLHVGQUFaO1FBQ0F5QixFQUFFLENBQUNFLE1BQUgsQ0FBVUQsSUFBVjtRQUNBZCxHQUFHLENBQUNlLE1BQUosQ0FBV0YsRUFBWDtNQUNELENBVkQsTUFZQTtRQUVFLG1CQUFJRixNQUFKLEVBQVlNLE9BQVosQ0FBb0IsVUFBQUMsSUFBSSxFQUFJO1VBQzFCLElBQU1DLElBQUksR0FBR2xDLFFBQVEsQ0FBQ2lCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBYjtVQUNBaUIsSUFBSSxDQUFDaEIsU0FBTCxDQUFlQyxHQUFmLENBQW1CLGtCQUFuQjtVQUVBLElBQU1nQixZQUFZLEdBQUduQyxRQUFRLENBQUNpQixhQUFULENBQXVCLEtBQXZCLENBQXJCO1VBQ0FrQixZQUFZLENBQUNqQixTQUFiLENBQXVCQyxHQUF2QixDQUEyQixlQUEzQjtVQUNBLElBQU1pQixHQUFHLEdBQUdwQyxRQUFRLENBQUNpQixhQUFULENBQXVCLEtBQXZCLENBQVo7VUFDQW1CLEdBQUcsQ0FBQ0MsR0FBSiw0QkFBNEJKLElBQUksQ0FBQyxPQUFELENBQWhDO1VBQ0FFLFlBQVksQ0FBQ0wsTUFBYixDQUFvQk0sR0FBcEI7VUFFQSxJQUFNRSxFQUFFLEdBQUd0QyxRQUFRLENBQUNpQixhQUFULENBQXVCLElBQXZCLENBQVg7VUFDQSxJQUFNc0IsQ0FBQyxHQUFHdkMsUUFBUSxDQUFDaUIsYUFBVCxDQUF1QixHQUF2QixDQUFWO1VBQ0FzQixDQUFDLENBQUNSLElBQUYsbUJBQWtCRSxJQUFJLENBQUMsSUFBRCxDQUF0QjtVQUNBTSxDQUFDLENBQUNwQyxJQUFGLEdBQVM4QixJQUFJLENBQUMsTUFBRCxDQUFiO1VBQ0FLLEVBQUUsQ0FBQ1IsTUFBSCxDQUFVUyxDQUFWO1VBRUEsSUFBTUMsT0FBTyxHQUFHeEMsUUFBUSxDQUFDaUIsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtVQUNBdUIsT0FBTyxDQUFDdEIsU0FBUixDQUFrQkMsR0FBbEIsQ0FBc0Isd0JBQXRCO1VBQ0EsSUFBTXNCLFVBQVUsR0FBR3pDLFFBQVEsQ0FBQ2lCLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBbkI7VUFDQSxJQUFNeUIsUUFBUSxHQUFHMUMsUUFBUSxDQUFDaUIsYUFBVCxDQUF1QixHQUF2QixDQUFqQjtVQUNBd0IsVUFBVSxDQUFDaEIsU0FBWCx3QkFBa0NRLElBQUksQ0FBQyxXQUFELENBQXRDO1VBQ0FTLFFBQVEsQ0FBQ2pCLFNBQVQsbUJBQThCUSxJQUFJLENBQUMsU0FBRCxDQUFKLEdBQWtCQSxJQUFJLENBQUMsU0FBRCxDQUF0QixHQUFvQyxJQUFsRTtVQUNBLElBQU1VLFFBQVEsR0FBRzNDLFFBQVEsQ0FBQ2lCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBakI7VUFDQTBCLFFBQVEsQ0FBQ3pCLFNBQVQsQ0FBbUJDLEdBQW5CLENBQXVCLCtCQUF2QjtVQUNBLElBQU15QixNQUFNLEdBQUc1QyxRQUFRLENBQUNpQixhQUFULENBQXVCLEdBQXZCLENBQWY7VUFDQTJCLE1BQU0sQ0FBQ25CLFNBQVAsR0FBbUJ2QixTQUFTLENBQUMrQixJQUFJLENBQUMsYUFBRCxDQUFMLEVBQXNCLEdBQXRCLENBQTVCO1VBR0FPLE9BQU8sQ0FBQ1YsTUFBUixDQUFlVyxVQUFmLEVBQTJCQyxRQUEzQjtVQUNBQyxRQUFRLENBQUNiLE1BQVQsQ0FBZ0JjLE1BQWhCO1VBQ0FWLElBQUksQ0FBQ0osTUFBTCxDQUFZSyxZQUFaLEVBQTBCRyxFQUExQixFQUE4QkUsT0FBOUIsRUFBdUNHLFFBQXZDO1VBQ0E1QixHQUFHLENBQUNlLE1BQUosQ0FBV2QsT0FBWDtVQUNBQSxPQUFPLENBQUNjLE1BQVIsQ0FBZUksSUFBZjtRQUNELENBakNEO01Ba0NEO0lBQ0osQ0F4REQsV0F5RE8sVUFBQ1csS0FBRCxFQUFXO01BQ2hCQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUYsS0FBWjtJQUNELENBM0REO0VBNERELENBL0VpRCxDQWlGbEQ7OztFQUNBLElBQU1HLGdCQUFnQixHQUFHaEQsUUFBUSxDQUFDVSxhQUFULENBQXVCLG1CQUF2QixDQUF6QjtFQUNBc0MsZ0JBQWdCLENBQUMvQyxnQkFBakIsQ0FBa0MsUUFBbEMsRUFBNEMsVUFBQ2dELENBQUQsRUFBTztJQUNqREEsQ0FBQyxDQUFDQyxjQUFGO0lBRUF0QyxHQUFHLENBQUN1QyxZQUFKLENBQWlCQyxHQUFqQixDQUFxQixNQUFyQixZQUFnQ0osZ0JBQWdCLENBQUNLLEtBQWpEO0lBQ0F2QyxpQkFBaUI7RUFDbEIsQ0FMRCxFQW5Ga0QsQ0EwRmxEOztFQUNBLElBQU13QyxrQkFBa0IsR0FBR3RELFFBQVEsQ0FBQ1UsYUFBVCxDQUF1QixxQkFBdkIsQ0FBM0I7RUFDQTRDLGtCQUFrQixDQUFDckQsZ0JBQW5CLENBQW9DLFFBQXBDLEVBQThDLFVBQUNnRCxDQUFELEVBQU87SUFDbkRBLENBQUMsQ0FBQ0MsY0FBRjtJQUNBLElBQU1LLE1BQU0sR0FBR0Qsa0JBQWtCLENBQUNELEtBQWxDO0lBQ0FqQyxLQUFLLFdBQUlULE9BQUosd0NBQXlDNEMsTUFBekMsRUFBTCxDQUNDbEMsSUFERCxDQUNNLFVBQUNDLFFBQUQsRUFBYztNQUNsQixPQUFPQSxRQUFRLENBQUNDLElBQVQsRUFBUDtJQUNELENBSEQsRUFJQ0YsSUFKRCxDQUlNLFVBQUNHLEdBQUQsRUFBUztNQUNiLElBQUdBLEdBQUcsS0FBSyxJQUFYLEVBQ0E7UUFDRVosR0FBRyxDQUFDdUMsWUFBSixXQUF3QixLQUF4QjtRQUNBckMsaUJBQWlCLENBQUMsTUFBRCxFQUFTLGVBQVQsQ0FBakI7TUFDRCxDQUpELE1BTUE7UUFDRTtRQUNBO1FBQ0EsSUFBTTBDLFNBQVMsR0FBR3hELFFBQVEsQ0FBQ2lCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBbEI7UUFDQXVDLFNBQVMsQ0FBQ0MsWUFBVixDQUF1QixJQUF2QixFQUE2QixXQUE3QjtRQUNBLElBQU1DLGNBQWMsR0FBRzFELFFBQVEsQ0FBQ2lCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBdkI7UUFDQXlDLGNBQWMsQ0FBQ0QsWUFBZixDQUE0QixVQUE1QixFQUF1QyxVQUF2QztRQUNBQyxjQUFjLENBQUNELFlBQWYsQ0FBNEIsVUFBNUIsRUFBdUMsVUFBdkM7UUFDQUMsY0FBYyxDQUFDdkQsSUFBZixHQUFzQixpQ0FBdEI7UUFDQXFELFNBQVMsQ0FBQ0csV0FBVixDQUFzQkQsY0FBdEI7UUFDQSxJQUFNaEMsTUFBTSxHQUFHRixHQUFmOztRQUNFLG1CQUFJRSxNQUFKLEVBQVlNLE9BQVosQ0FBb0IsVUFBQTRCLEdBQUcsRUFBSTtVQUN6QixJQUFNQyxTQUFTLEdBQUc3RCxRQUFRLENBQUNpQixhQUFULENBQXVCLFFBQXZCLENBQWxCO1VBQ0E0QyxTQUFTLENBQUNSLEtBQVYsR0FBa0JPLEdBQUcsQ0FBQyxNQUFELENBQXJCO1VBQ0FDLFNBQVMsQ0FBQzFELElBQVYsR0FBaUJ5RCxHQUFHLENBQUMsTUFBRCxDQUFwQjtVQUNBSixTQUFTLENBQUNHLFdBQVYsQ0FBc0JFLFNBQXRCO1FBQ0QsQ0FMRDs7UUFPRnBELFNBQVMsQ0FBQ3FCLE1BQVYsQ0FBaUIwQixTQUFqQjtRQUNBRixrQkFBa0IsQ0FBQ3JELGdCQUFuQixDQUFvQyxRQUFwQyxFQUE4QyxVQUFDZ0QsQ0FBRCxFQUFPO1VBQ25EQSxDQUFDLENBQUNDLGNBQUY7VUFDQU0sU0FBUyxDQUFDTSxNQUFWO1FBQ0QsQ0FIRCxFQW5CRixDQXdCRTs7UUFDQSxJQUFNQyxlQUFlLEdBQUcvRCxRQUFRLENBQUNVLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBeEI7UUFDQXFELGVBQWUsQ0FBQzlELGdCQUFoQixDQUFpQyxRQUFqQyxFQUEyQyxVQUFDZ0QsQ0FBRCxFQUFPO1VBQ2hEQSxDQUFDLENBQUNDLGNBQUY7VUFDQXRDLEdBQUcsQ0FBQ3VDLFlBQUosQ0FBaUJDLEdBQWpCLENBQXFCLEtBQXJCLFlBQStCVyxlQUFlLENBQUNWLEtBQS9DO1VBQ0F2QyxpQkFBaUI7UUFDbEIsQ0FKRDtNQU9EO0lBQ0YsQ0E3Q0QsV0E4Q08sVUFBQytCLEtBQUQsRUFBVztNQUNoQkMsT0FBTyxDQUFDQyxHQUFSLENBQVlGLEtBQVo7SUFDRCxDQWhERDtFQWlERCxDQXBERCxFQTVGa0QsQ0FrSmxEOztFQUNDLElBQU1tQixZQUFZLEdBQUdoRSxRQUFRLENBQUNpRSxnQkFBVCxDQUEwQix1QkFBMUIsQ0FBckI7RUFDQSxJQUFNQyxhQUFhLEdBQUcsRUFBdEI7O0VBcEppRCwyQ0FxSjFCRixZQXJKMEI7RUFBQTs7RUFBQTtJQUFBO01BQUEsSUFxSnRDRyxRQXJKc0M7TUFzSmhEQSxRQUFRLENBQUNsRSxnQkFBVCxDQUEwQixRQUExQixFQUFvQyxVQUFDZ0QsQ0FBRCxFQUFPO1FBQ3pDQSxDQUFDLENBQUNDLGNBQUY7O1FBQ0EsSUFBR0QsQ0FBQyxDQUFDbUIsTUFBRixDQUFTQyxPQUFaLEVBQ0E7VUFDRUgsYUFBYSxDQUFDSSxJQUFkLENBQW1CSCxRQUFRLENBQUNkLEtBQTVCO1FBQ0Q7O1FBQ0QsSUFBR0osQ0FBQyxDQUFDbUIsTUFBRixDQUFTQyxPQUFULEtBQXFCLEtBQXhCLEVBQ0E7VUFDRTtVQUNBLElBQU1FLEtBQUssR0FBR0wsYUFBYSxDQUFDTSxPQUFkLENBQXNCTCxRQUFRLENBQUNkLEtBQS9CLENBQWQ7O1VBQ0EsSUFBR2tCLEtBQUssR0FBRyxDQUFDLENBQVosRUFDQTtZQUNFTCxhQUFhLENBQUNPLE1BQWQsQ0FBcUJGLEtBQXJCLEVBQTRCLENBQTVCO1VBQ0Q7UUFDRjs7UUFFRDNELEdBQUcsQ0FBQ3VDLFlBQUosQ0FBaUJDLEdBQWpCLENBQXFCLEtBQXJCLFlBQStCYyxhQUEvQjtRQUNBcEQsaUJBQWlCO01BQ2xCLENBbEJEO0lBdEpnRDs7SUFxSmpELG9EQUFxQztNQUFBO0lBb0JyQztFQXpLaUQ7SUFBQTtFQUFBO0lBQUE7RUFBQTtBQTBLbkQsQ0ExS0QiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvX2ZpbHRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCAoKSA9PiB7XG5cbiAgZnVuY3Rpb24gc3BsaXRUZXh0KHRleHQsIGNvdW50KXtcbiAgICBjb25zdCBzdHJpcGVkVGV4dCA9IHRleHQucmVwbGFjZSggLyg8KFtePl0rKT4pL2lnLCAnJyk7XG4gICAgcmV0dXJuIHN0cmlwZWRUZXh0LnNsaWNlKDAsIGNvdW50KSArICh0ZXh0Lmxlbmd0aCA+IGNvdW50ID8gXCIuLi5cIiA6IFwiXCIpO1xuICB9XG5cbiAgY29uc3QgZmlsdGVyRGl2ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2ZpbHRlcnMnKTsgIFxuICBcbiAgY29uc3QgYmFzZVVybCA9ICdodHRwOi8vMTI3LjAuMC4xOjgwMDAnOyBcbiAgY29uc3QgdXJsPSBuZXcgVVJMKGAke2Jhc2VVcmx9L2FwaS9maWx0ZXIvYCk7XG5cbiAgLy9mZXRjaCBkdSBhcGkgZmlsdGVyIGV0IGNyw6lhdGlvbiBkZSBsYSBjYXJ0ZSAnaHVudCcgZW4gZm9uY3Rpb24gZHUgcmVzXG4gIGZ1bmN0aW9uIHNob3dGaWx0ZXJlZEh1bnRzKClcbiAge1xuICAgIGNvbnN0IGRpdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNpbmRleEh1bnRzJyk7XG4gICAgY29uc3Qgc2VjdGlvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NlY3Rpb24nKTsgXG4gICAgc2VjdGlvbi5jbGFzc0xpc3QuYWRkKCdob3Jpem9udGFsLWluZm8nKTsgXG5cbiAgICBmZXRjaCh1cmwpXG4gICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICByZXR1cm4gcmVzcG9uc2UuanNvbigpOyBcbiAgICB9KVxuICAgIC50aGVuKChyZXMpID0+IHsgIFxuICAgICAgICBkaXYuaW5uZXJIVE1MID0gXCJcIjsgIFxuICAgICAgICBjb25zdCByZXN1bHQgPSByZXM7IFxuICAgICAgICBpZihyZXN1bHQubGVuZ3RoID09PSAwKVxuICAgICAgICB7XG4gICAgICAgICAgY29uc3QgcDEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTsgXG4gICAgICAgICAgY29uc3QgcDIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTsgXG4gICAgICAgICAgY29uc3QgYU5ldyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJhXCIpOyBcbiAgICAgICAgICBkaXYuYXBwZW5kKCdSaWVuIMOgIHZvaXIgaWNpIHBvdXIgbGUgbW9tZW50LiDwn5ieJywgcDEpO1xuICAgICAgICAgIGFOZXcuaHJlZiA9ICcvaHVudC9uZXcnO1xuICAgICAgICAgIGFOZXcudGV4dCA9ICdTaSB2b3VzIGF2ZXogY29ubmFpc3NhbmNlIGRcXCd1bmUgY2hhc3NlLCB2b3VzIHBvdXZleiBsYSBwcm9wb3NlciBlbiBjbGlxdWFudCBpY2kgISDwn5iKJztcbiAgICAgICAgICBwMi5hcHBlbmQoYU5ldyk7IFxuICAgICAgICAgIGRpdi5hcHBlbmQocDIpO1xuICAgICAgICB9IFxuICAgICAgICBlbHNlXG4gICAgICAgIHtcblxuICAgICAgICAgIFsuLi5yZXN1bHRdLmZvckVhY2goaHVudCA9PiB7XG4gICAgICAgICAgICBjb25zdCBjYXJkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7IFxuICAgICAgICAgICAgY2FyZC5jbGFzc0xpc3QuYWRkKCdob3Jpem9udGFsLWNhcmRzJyk7IFxuXG4gICAgICAgICAgICBjb25zdCBpbWdDb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTsgXG4gICAgICAgICAgICBpbWdDb250YWluZXIuY2xhc3NMaXN0LmFkZCgnaW1nLWNvbnRhaW5lcicpOyBcbiAgICAgICAgICAgIGNvbnN0IGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuICAgICAgICAgICAgaW1nLnNyYyA9IGAvdXBsb2Fkcy9odW50cy8ke2h1bnRbJ2ltYWdlJ119YDsgXG4gICAgICAgICAgICBpbWdDb250YWluZXIuYXBwZW5kKGltZyk7IFxuXG4gICAgICAgICAgICBjb25zdCBoMyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2gzJyk7IFxuICAgICAgICAgICAgY29uc3QgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTsgXG4gICAgICAgICAgICBhLmhyZWYgPSBgL2h1bnQvJHtodW50WydpZCddfWA7IFxuICAgICAgICAgICAgYS50ZXh0ID0gaHVudFsnbmFtZSddOyBcbiAgICAgICAgICAgIGgzLmFwcGVuZChhKTsgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNvbnN0IGRpdkRhdGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTsgXG4gICAgICAgICAgICBkaXZEYXRlLmNsYXNzTGlzdC5hZGQoJ2hvcml6b250YWwtY2FyZHNfX2RhdGUnKTsgXG4gICAgICAgICAgICBjb25zdCBwQmVnaW5EYXRlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpOyBcbiAgICAgICAgICAgIGNvbnN0IHBFbmREYXRlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICAgICAgcEJlZ2luRGF0ZS5pbm5lckhUTUwgPSBgRMOpYnV0IDogJHtodW50WydiZWdpbkRhdGUnXX1gOyBcbiAgICAgICAgICAgIHBFbmREYXRlLmlubmVySFRNTCA9IGBGaW4gOiAke2h1bnRbJ2VuZERhdGUnXSA/IGh1bnRbJ2VuZERhdGUnXSA6ICdOQyd9YDsgXG4gICAgICAgICAgICBjb25zdCBkaXZEZXNjciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpOyBcbiAgICAgICAgICAgIGRpdkRlc2NyLmNsYXNzTGlzdC5hZGQoJ2hvcml6b250YWwtY2FyZHNfX2Rlc2NyaXB0aW9uJyk7IFxuICAgICAgICAgICAgY29uc3QgcERlc2NyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpOyBcbiAgICAgICAgICAgIHBEZXNjci5pbm5lckhUTUwgPSBzcGxpdFRleHQoaHVudFsnZGVzY3JpcHRpb24nXSwgMjUwKTsgXG5cblxuICAgICAgICAgICAgZGl2RGF0ZS5hcHBlbmQocEJlZ2luRGF0ZSwgcEVuZERhdGUpOyBcbiAgICAgICAgICAgIGRpdkRlc2NyLmFwcGVuZChwRGVzY3IpOyBcbiAgICAgICAgICAgIGNhcmQuYXBwZW5kKGltZ0NvbnRhaW5lciwgaDMsIGRpdkRhdGUsIGRpdkRlc2NyKTsgXG4gICAgICAgICAgICBkaXYuYXBwZW5kKHNlY3Rpb24pO1xuICAgICAgICAgICAgc2VjdGlvbi5hcHBlbmQoY2FyZCk7IFxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9KVxuICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8vRklMVFJFIFBBUiBEQVRFIChham91dCBkYXRlIGNvbW1lIHVybCBwYXJhbSlcbiAgY29uc3Qgc2VsZWN0SHVudEJ5RGF0ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2VsZWN0SHVudEJ5RGF0ZVwiKTtcbiAgc2VsZWN0SHVudEJ5RGF0ZS5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIChlKSA9PiB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgdXJsLnNlYXJjaFBhcmFtcy5zZXQoJ2RhdGUnLCBgJHtzZWxlY3RIdW50QnlEYXRlLnZhbHVlfWApO1xuICAgIHNob3dGaWx0ZXJlZEh1bnRzKCk7IFxuICB9KTtcblxuICAvL1JFQ0hFUkNIRSBEUFQgUEFSIFJFR0lPTlxuICBjb25zdCBzZWxlY3RIdW50QnlSZWdpb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc2VsZWN0SHVudEJ5UmVnaW9uJyk7IFxuICBzZWxlY3RIdW50QnlSZWdpb24uYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZSkgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCByZWdpb24gPSBzZWxlY3RIdW50QnlSZWdpb24udmFsdWU7IFxuICAgIGZldGNoKGAke2Jhc2VVcmx9L2FwaS9maWx0ZXIvcmVnaW9ucz9yZWdpb249JHtyZWdpb259YClcbiAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7IFxuICAgIH0pXG4gICAgLnRoZW4oKHJlcykgPT4ge1xuICAgICAgaWYocmVzID09PSAnRlInKVxuICAgICAge1xuICAgICAgICB1cmwuc2VhcmNoUGFyYW1zLmRlbGV0ZSgnZHB0Jyk7IFxuICAgICAgICBzaG93RmlsdGVyZWRIdW50cygnZGF0ZScsICdkZXNjQmVnaW5EYXRlJyk7IFxuICAgICAgfVxuICAgICAgZWxzZVxuICAgICAge1xuICAgICAgICAvL3JlY3Vww6lyYXRpb24gZGVzIGRwdHMgcGFyIHLDqWdpb24gXG4gICAgICAgIC8vY3JlYXRpb24gZHUgc2VsZWN0IGV0IGRlcyBvcHRpb25zIGVuIGdhcmRhbnQgbGUgY29kZSBldCBsZSBub20gZHUgZHB0IFxuICAgICAgICBjb25zdCBzZWxlY3REcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic2VsZWN0XCIpO1xuICAgICAgICBzZWxlY3REcHQuc2V0QXR0cmlidXRlKFwiaWRcIiwgXCJzZWxlY3REcHRcIik7XG4gICAgICAgIGNvbnN0IG9wdGlvbkRpc2FibGVkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcIm9wdGlvblwiKTsgXG4gICAgICAgIG9wdGlvbkRpc2FibGVkLnNldEF0dHJpYnV0ZShcImRpc2FibGVkXCIsXCJkaXNhYmxlZFwiKTtcbiAgICAgICAgb3B0aW9uRGlzYWJsZWQuc2V0QXR0cmlidXRlKFwic2VsZWN0ZWRcIixcInNlbGVjdGVkXCIpO1xuICAgICAgICBvcHRpb25EaXNhYmxlZC50ZXh0ID0gXCItLVPDqWxlY3Rpb25uZXIgdW4gZMOpcGFydGVtZW50LS1cIjsgXG4gICAgICAgIHNlbGVjdERwdC5hcHBlbmRDaGlsZChvcHRpb25EaXNhYmxlZCk7IFxuICAgICAgICBjb25zdCByZXN1bHQgPSByZXM7IFxuICAgICAgICAgIFsuLi5yZXN1bHRdLmZvckVhY2goZHB0ID0+IHtcbiAgICAgICAgICAgIGNvbnN0IG9wdGlvbkRwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJvcHRpb25cIik7XG4gICAgICAgICAgICBvcHRpb25EcHQudmFsdWUgPSBkcHRbJ2NvZGUnXTsgIFxuICAgICAgICAgICAgb3B0aW9uRHB0LnRleHQgPSBkcHRbJ25hbWUnXTsgIFxuICAgICAgICAgICAgc2VsZWN0RHB0LmFwcGVuZENoaWxkKG9wdGlvbkRwdCk7IFxuICAgICAgICAgIH0pXG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJEaXYuYXBwZW5kKHNlbGVjdERwdCk7IFxuICAgICAgICBzZWxlY3RIdW50QnlSZWdpb24uYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZSkgPT4ge1xuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICBzZWxlY3REcHQucmVtb3ZlKCk7IFxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIC8vRklMVFJFIFBBUiBEUFQgKGFqb3V0IGRwdCB1cmwgcGFyYW0pXG4gICAgICAgIGNvbnN0IHNlbGVjdEh1bnRCeURwdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzZWxlY3REcHQnKTtcbiAgICAgICAgc2VsZWN0SHVudEJ5RHB0LmFkZEV2ZW50TGlzdGVuZXIoXCJjaGFuZ2VcIiwgKGUpID0+IHtcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7IFxuICAgICAgICAgIHVybC5zZWFyY2hQYXJhbXMuc2V0KCdkcHQnLCBgJHtzZWxlY3RIdW50QnlEcHQudmFsdWV9YCk7XG4gICAgICAgICAgc2hvd0ZpbHRlcmVkSHVudHMoKTsgXG4gICAgICAgIH0pO1xuXG4gICAgICAgIFxuICAgICAgfVxuICAgIH0pXG4gICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH0pO1xuICB9KTtcblxuICAvL0ZJTFRSRSBQQVIgQ0FURUdPUklFIChham91dCBjYXQgdXJsIHBhcmFtKVxuICAgY29uc3QgY2hlY2tib3hMaXN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNoZWNrYm94SHVudENhdGVnb3J5Jyk7IFxuICAgY29uc3QgY2hlY2tlZFZhbHVlcyA9IFtdOyBcbiAgIGZvciAoY29uc3QgY2hlY2tib3ggb2YgY2hlY2tib3hMaXN0KSB7XG4gICAgY2hlY2tib3guYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZSkgPT4ge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpOyBcbiAgICAgIGlmKGUudGFyZ2V0LmNoZWNrZWQpXG4gICAgICB7XG4gICAgICAgIGNoZWNrZWRWYWx1ZXMucHVzaChjaGVja2JveC52YWx1ZSk7IFxuICAgICAgfSBcbiAgICAgIGlmKGUudGFyZ2V0LmNoZWNrZWQgPT09IGZhbHNlKVxuICAgICAge1xuICAgICAgICAvL3JldHJhaXQgZGUgbGEgdmFsdWUgZHUgdGFibGVhdSBzaSBvbiByZXRpcmUgbGUgY2hlY2tcbiAgICAgICAgY29uc3QgaW5kZXggPSBjaGVja2VkVmFsdWVzLmluZGV4T2YoY2hlY2tib3gudmFsdWUpO1xuICAgICAgICBpZihpbmRleCA+IC0xKVxuICAgICAgICB7XG4gICAgICAgICAgY2hlY2tlZFZhbHVlcy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHVybC5zZWFyY2hQYXJhbXMuc2V0KCdjYXQnLCBgJHtjaGVja2VkVmFsdWVzfWApO1xuICAgICAgc2hvd0ZpbHRlcmVkSHVudHMoKTsgXG4gICAgfSk7XG4gIH1cbn0pO1xuXG4iXSwibmFtZXMiOlsiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwic3BsaXRUZXh0IiwidGV4dCIsImNvdW50Iiwic3RyaXBlZFRleHQiLCJyZXBsYWNlIiwic2xpY2UiLCJsZW5ndGgiLCJmaWx0ZXJEaXYiLCJxdWVyeVNlbGVjdG9yIiwiYmFzZVVybCIsInVybCIsIlVSTCIsInNob3dGaWx0ZXJlZEh1bnRzIiwiZGl2Iiwic2VjdGlvbiIsImNyZWF0ZUVsZW1lbnQiLCJjbGFzc0xpc3QiLCJhZGQiLCJmZXRjaCIsInRoZW4iLCJyZXNwb25zZSIsImpzb24iLCJyZXMiLCJpbm5lckhUTUwiLCJyZXN1bHQiLCJwMSIsInAyIiwiYU5ldyIsImFwcGVuZCIsImhyZWYiLCJmb3JFYWNoIiwiaHVudCIsImNhcmQiLCJpbWdDb250YWluZXIiLCJpbWciLCJzcmMiLCJoMyIsImEiLCJkaXZEYXRlIiwicEJlZ2luRGF0ZSIsInBFbmREYXRlIiwiZGl2RGVzY3IiLCJwRGVzY3IiLCJlcnJvciIsImNvbnNvbGUiLCJsb2ciLCJzZWxlY3RIdW50QnlEYXRlIiwiZSIsInByZXZlbnREZWZhdWx0Iiwic2VhcmNoUGFyYW1zIiwic2V0IiwidmFsdWUiLCJzZWxlY3RIdW50QnlSZWdpb24iLCJyZWdpb24iLCJzZWxlY3REcHQiLCJzZXRBdHRyaWJ1dGUiLCJvcHRpb25EaXNhYmxlZCIsImFwcGVuZENoaWxkIiwiZHB0Iiwib3B0aW9uRHB0IiwicmVtb3ZlIiwic2VsZWN0SHVudEJ5RHB0IiwiY2hlY2tib3hMaXN0IiwicXVlcnlTZWxlY3RvckFsbCIsImNoZWNrZWRWYWx1ZXMiLCJjaGVja2JveCIsInRhcmdldCIsImNoZWNrZWQiLCJwdXNoIiwiaW5kZXgiLCJpbmRleE9mIiwic3BsaWNlIl0sInNvdXJjZVJvb3QiOiIifQ==