(self["webpackChunk"] = self["webpackChunk"] || []).push([["form"],{

/***/ "./assets/form.js":
/*!************************!*\
  !*** ./assets/form.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _js_form_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/_form.js */ "./assets/js/_form.js");
/* harmony import */ var _js_form_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_js_form_js__WEBPACK_IMPORTED_MODULE_0__);


/***/ }),

/***/ "./assets/js/_form.js":
/*!****************************!*\
  !*** ./assets/js/_form.js ***!
  \****************************/
/***/ (() => {

document.addEventListener("DOMContentLoaded", function () {
  //Affichage ou suppression de l'élement du dom en fonction du choix dans le form
  function toggleOption(select, element, value) {
    if (select != null) {
      element.style.display = "none";
      select.addEventListener("change", function (e) {
        if (e.target.value === value) {
          element.style.display = "block";
        } else {
          element.style.display = "none";
        }
      });
    }
  }

  var selectIsPayable = document.querySelector("#hunt_isPayable");
  var entryFee = document.querySelector("#huntFormEntryFee");
  toggleOption(selectIsPayable, entryFee, '1');
  var selectEndDate = document.querySelector("#selectHuntNewEndDate");
  var endDate = document.querySelector("#huntFormEndDate");
  toggleOption(selectEndDate, endDate, '1');
  var selectIsLocated = document.querySelector("#hunt_isLocated");
  var department = document.querySelector("#huntFormDpt");
  toggleOption(selectIsLocated, department, '0');
});

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ var __webpack_exports__ = (__webpack_exec__("./assets/form.js"));
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUFBLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQU07RUFDbEQ7RUFDQSxTQUFTQyxZQUFULENBQXNCQyxNQUF0QixFQUE4QkMsT0FBOUIsRUFBdUNDLEtBQXZDLEVBQ0E7SUFDRSxJQUFJRixNQUFNLElBQUksSUFBZCxFQUFvQjtNQUNsQkMsT0FBTyxDQUFDRSxLQUFSLENBQWNDLE9BQWQsR0FBd0IsTUFBeEI7TUFDQUosTUFBTSxDQUFDRixnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxVQUFDTyxDQUFELEVBQU87UUFDdkMsSUFBSUEsQ0FBQyxDQUFDQyxNQUFGLENBQVNKLEtBQVQsS0FBbUJBLEtBQXZCLEVBQThCO1VBQzVCRCxPQUFPLENBQUNFLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixPQUF4QjtRQUNELENBRkQsTUFFTztVQUNMSCxPQUFPLENBQUNFLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixNQUF4QjtRQUNEO01BQ0YsQ0FORDtJQU9EO0VBQ0Y7O0VBQ0QsSUFBTUcsZUFBZSxHQUFHVixRQUFRLENBQUNXLGFBQVQsQ0FBdUIsaUJBQXZCLENBQXhCO0VBQ0EsSUFBTUMsUUFBUSxHQUFHWixRQUFRLENBQUNXLGFBQVQsQ0FBdUIsbUJBQXZCLENBQWpCO0VBQ0FULFlBQVksQ0FBQ1EsZUFBRCxFQUFrQkUsUUFBbEIsRUFBNEIsR0FBNUIsQ0FBWjtFQUVBLElBQU1DLGFBQWEsR0FBR2IsUUFBUSxDQUFDVyxhQUFULENBQXVCLHVCQUF2QixDQUF0QjtFQUNBLElBQU1HLE9BQU8sR0FBR2QsUUFBUSxDQUFDVyxhQUFULENBQXVCLGtCQUF2QixDQUFoQjtFQUNBVCxZQUFZLENBQUNXLGFBQUQsRUFBZ0JDLE9BQWhCLEVBQXlCLEdBQXpCLENBQVo7RUFFQSxJQUFNQyxlQUFlLEdBQUdmLFFBQVEsQ0FBQ1csYUFBVCxDQUF1QixpQkFBdkIsQ0FBeEI7RUFDQSxJQUFNSyxVQUFVLEdBQUdoQixRQUFRLENBQUNXLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBbkI7RUFDQVQsWUFBWSxDQUFDYSxlQUFELEVBQWtCQyxVQUFsQixFQUE4QixHQUE5QixDQUFaO0FBRUQsQ0EzQkQiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvZm9ybS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvX2Zvcm0uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICcuL2pzL19mb3JtLmpzJzsiLCJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCAoKSA9PiB7XG4gIC8vQWZmaWNoYWdlIG91IHN1cHByZXNzaW9uIGRlIGwnw6lsZW1lbnQgZHUgZG9tIGVuIGZvbmN0aW9uIGR1IGNob2l4IGRhbnMgbGUgZm9ybVxuICBmdW5jdGlvbiB0b2dnbGVPcHRpb24oc2VsZWN0LCBlbGVtZW50LCB2YWx1ZSlcbiAge1xuICAgIGlmIChzZWxlY3QgIT0gbnVsbCkge1xuICAgICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgICBzZWxlY3QuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZSkgPT4ge1xuICAgICAgICBpZiAoZS50YXJnZXQudmFsdWUgPT09IHZhbHVlKSB7XG4gICAgICAgICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cbiAgY29uc3Qgc2VsZWN0SXNQYXlhYmxlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNodW50X2lzUGF5YWJsZVwiKTtcbiAgY29uc3QgZW50cnlGZWUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2h1bnRGb3JtRW50cnlGZWVcIik7XG4gIHRvZ2dsZU9wdGlvbihzZWxlY3RJc1BheWFibGUsIGVudHJ5RmVlLCAnMScpOyBcblxuICBjb25zdCBzZWxlY3RFbmREYXRlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzZWxlY3RIdW50TmV3RW5kRGF0ZVwiKTtcbiAgY29uc3QgZW5kRGF0ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaHVudEZvcm1FbmREYXRlXCIpO1xuICB0b2dnbGVPcHRpb24oc2VsZWN0RW5kRGF0ZSwgZW5kRGF0ZSwgJzEnKTsgXG5cbiAgY29uc3Qgc2VsZWN0SXNMb2NhdGVkID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNodW50X2lzTG9jYXRlZFwiKTtcbiAgY29uc3QgZGVwYXJ0bWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaHVudEZvcm1EcHRcIik7XG4gIHRvZ2dsZU9wdGlvbihzZWxlY3RJc0xvY2F0ZWQsIGRlcGFydG1lbnQsICcwJyk7IFxuXG59KTtcbiJdLCJuYW1lcyI6WyJkb2N1bWVudCIsImFkZEV2ZW50TGlzdGVuZXIiLCJ0b2dnbGVPcHRpb24iLCJzZWxlY3QiLCJlbGVtZW50IiwidmFsdWUiLCJzdHlsZSIsImRpc3BsYXkiLCJlIiwidGFyZ2V0Iiwic2VsZWN0SXNQYXlhYmxlIiwicXVlcnlTZWxlY3RvciIsImVudHJ5RmVlIiwic2VsZWN0RW5kRGF0ZSIsImVuZERhdGUiLCJzZWxlY3RJc0xvY2F0ZWQiLCJkZXBhcnRtZW50Il0sInNvdXJjZVJvb3QiOiIifQ==